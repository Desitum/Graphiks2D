package app.fireflygames.library.math

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle
import kotlin.math.min

/**
 * Triangle
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/22/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */
class Triangle(var x1: Float,
               var y1: Float,
               var x2: Float,
               var y2: Float,
               var x3: Float,
               var y3: Float) {

    fun shift(x: Float, y: Float) {
        x1 += x
        y1 += y
        x2 += x
        y2 += y
        x3 += x
        y3 += y
    }

    fun shiftX(x: Float) { shift(x, 0f) }

    fun shiftY(y: Float) { shift(0f, y) }

    fun setPosition(x: Float, y: Float) {
        val difX = x - arrayOf(x1, x2, x3).min()!!
        val difY = y - arrayOf(y1, y2, y3).min()!!
        shift(difX, difY)
        rect.setPosition(x, y)
    }

    val rect: Rectangle

    val segments: Array<LinearSegment>
        get() {
            return arrayOf(
                    LinearSegment(x1, y1, x2, y2),
                    LinearSegment(x2, y2, x3, y3),
                    LinearSegment(x3, y3, x1, y1)
            )
        }

    init {
        val minX = arrayOf(x1, x2, x3).min()!!
        val minY = arrayOf(y1, y2, y3).min()!!
        val maxX = arrayOf(x1, x2, x3).max()!!
        val maxY = arrayOf(y1, y2, y3).max()!!
        rect = Rectangle(
                minX,
                minY,
                maxX - minX,
                maxY - minY
        )
    }
}

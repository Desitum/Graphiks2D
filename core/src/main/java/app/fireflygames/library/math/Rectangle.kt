package app.fireflygames.library.math

import com.badlogic.gdx.math.Rectangle

/**
 * Rectangle.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/22/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */

val Rectangle.segments: Array<LinearSegment>
    get() = arrayOf(
            LinearSegment(x, y, x + width, y),
            LinearSegment(x + width, y, x + width, y + height),
            LinearSegment(x + width, y + height, x, y + height),
            LinearSegment(x, y + height, x, y)
    )

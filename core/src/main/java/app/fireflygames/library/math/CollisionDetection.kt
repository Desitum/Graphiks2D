package app.fireflygames.library.math

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector3
import app.fireflygames.library.game_objects.GameObject

/**
 * Created by Zmyth97 on 2/9/2015
 */
object CollisionDetection {

    @JvmStatic fun pointInRectangle(rect: Rectangle, point: Vector3): Boolean {
        val rw: Float = rect.getWidth()
        val rh: Float = rect.getHeight()
        val rx: Float = rect.getX()
        val ry: Float = rect.getY()
        val px: Float = point.x
        val py: Float = point.y

        return px >= rx && px <= rw + rx && py >= ry && py <= ry + rh
    }

    @JvmStatic fun overlapRectangles(r1: Rectangle, r2: Rectangle): Boolean {
        return r1.x < r2.x + r2.width && r1.x + r1.width > r2.x && r1.y < r2.y + r2.height && r1.y + r1.height > r2.y
    }

    @JvmStatic fun linesTouch(line1: LinearSegment, line2: LinearSegment): Boolean {
        return Intersector.intersectSegments(line1.x1, line1.y1, line1.x2, line1.y2, line2.x1, line2.y1, line2.x2, line2.y2, null)
    }

    @JvmStatic fun triangleOverlapsRectangle(rect: Rectangle, triangle: Triangle): Boolean {
        if (!overlapRectangles(rect, triangle.rect)) return false
        for (rectLine in rect.segments) {
            for (triangleLine in triangle.segments) {
                if (linesTouch(rectLine, triangleLine)){
                    return true
                }
            }
        }
        return false
    }

    @JvmStatic fun spritesTouching(s1: Sprite, s2: Sprite): Boolean {
        return overlapRectangles(s1.boundingRectangle, s2.boundingRectangle)
    }

    @JvmStatic fun gameObjectsTouching(object1: GameObject, object2: GameObject): Boolean {
        return overlapRectangles(object1.boundingRectangle, object2.boundingRectangle)
    }
}

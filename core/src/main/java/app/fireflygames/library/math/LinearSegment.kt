package app.fireflygames.library.math

import app.fireflygames.library.game.VERBOSE_LOGGING
import app.fireflygames.library.logging.Log
import com.badlogic.gdx.math.Vector2
import kotlin.math.min

/**
 * LinearSegment.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/22/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */
class LinearSegment(var x1: Float,
                    var y1: Float,
                    var x2: Float,
                    var y2: Float) {
    val p1: Vector2
        get() = Vector2(x1, y1)
    val p2: Vector2
        get() = Vector2(x2, y2)

    var x: Float = min(x1, x2)
        get() = min(x1, x2)
        set(value) {
            x1 += value - field
            x2 += value - field
            field = value
        }

    var y: Float = min(y1, y2)
        get() = min(y1, y2)
        set(value) {
            y1 += value - field
            y2 += value - field
            field = value
        }

    init {
        if (VERBOSE_LOGGING)
            Log.callStack(this)
    }
}

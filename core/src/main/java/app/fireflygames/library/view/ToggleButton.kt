package app.fireflygames.library.view

import app.fireflygames.library.drawing.Drawable
import app.fireflygames.library.game.World

/**
 * Created by kodyvanry on 5/1/17
 */

open class ToggleButton constructor(world: World, layoutConstraints: LayoutConstraints, onDrawable: Drawable, offDrawable: Drawable) : View(world, layoutConstraints) {

    var toggled = false
        set(value) {
            field = value
            backgroundDrawable = if (field) onDrawable else offDrawable
            onToggle(this)
        }

    var onToggle: (ToggleButton) -> Unit = {}

    // DRAWING
    var onDrawable: Drawable = onDrawable
        set(value) {
            backgroundDrawable = backgroundDrawable ?: value
            field = value
        }

    var offDrawable: Drawable = offDrawable
        set(value) {
            backgroundDrawable = backgroundDrawable ?: value
            field = value
        }

    init {
        clickable = true
        backgroundDrawable = offDrawable
    }

    override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        if (touchEvent.action == TouchEvent.Action.UP) {
            if (isTouching(touchEvent))
                toggled = !toggled
        }
        return super.onTouchEvent(touchEvent)
    }
}

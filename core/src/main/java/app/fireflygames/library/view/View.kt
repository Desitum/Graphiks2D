package app.fireflygames.library.view

import app.fireflygames.library.animation.Animation
import app.fireflygames.library.animation.Animator
import app.fireflygames.library.drawing.Drawable
import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.game.World
import app.fireflygames.library.game_objects.Visibility
import app.fireflygames.library.view.LayoutConstraints.Companion.NOT_SET
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.NumberUtils
import com.badlogic.gdx.utils.viewport.Viewport
import org.jetbrains.annotations.NotNull
import java.util.*

/**
 * View
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/13/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
@Suppress("Unused", "PLUGIN_WARNING", "MemberVisibilityCanBePrivate")
open class View(var world: World, layoutConstraints: LayoutConstraints?) : G2DSprite() {

    @NotNull
    var layoutConstraints: LayoutConstraints = layoutConstraints
            ?: LayoutConstraints(0f, 0f, 0f, 0f)
        set(value) {
            field = value
            dirty = true
        }

    // TOUCH
    var focus = false
    var enabled = true
    var focusable = true
    var clickable = false

    var onClick: (View) -> Unit = {}
        set(value) {
            field = value
            clickable = true
        }

    var onTouch: (View, TouchEvent) -> Unit = { _, _ -> }

    // DRAWING
    var backgroundDrawable: Drawable? = null
        set(value) {
            field = value?.clone()
        }

    open var visibility = Visibility.VISIBLE
        set(value) {
            field = value
            dirty = true
        }

    @Transient
    private var dirty = true
    var parent: ViewGroup? = null

    var id: Int = 0
    var name: String? = null
    var animators: MutableList<Animator> = ArrayList()

    init {
        setOriginCenter()
    }

    // -----------------------------
    // region Animation methods
    // -----------------------------
    override fun update(delta: Float) {
        if (dirty)
            dispatchLayout()
        animators.forEach {
            it.update(delta)
            invalidate()
        }
        animators.removeAll { it.didFinish() }
    }

    fun addAnimation(animator: Animator) {
        if (!animators.contains(animator))
            animators.add(animator)
    }

    fun startAnimator(animator: Animator) {
        addAnimation(animator)
        animator.start()
    }

    fun clearAnimators() {
        animators.clear()
    }
    // -----------------------------
    // endregion
    // -----------------------------

    // -----------------------------
    // region Layout methods
    // -----------------------------
    open fun dispatchLayout() {
        if (visibility != Visibility.GONE) {
            dispatchSizeLayout(layoutConstraints)
            dispatchPositionLayout(layoutConstraints)
        }
        dirty = false
    }

    open fun dispatchSizeLayout(layoutConstraints: LayoutConstraints) {
        super.setWidth(when (layoutConstraints.width) {
            MATCH_PARENT -> measureWidthMatchParent(layoutConstraints)
            WRAP_CONTENT -> measureWidthWrapContent(layoutConstraints)
            else -> layoutConstraints.width
        })
        super.setHeight(when (layoutConstraints.height) {
            MATCH_PARENT -> measureHeightMatchParent(layoutConstraints)
            WRAP_CONTENT -> measureHeightWrapContent(layoutConstraints)
            else -> layoutConstraints.height
        })
    }

    open fun dispatchPositionLayout(layoutConstraints: LayoutConstraints) {
        super.setX(
                when {
                    layoutConstraints.alignParentStart -> measureXAlignParentStart(layoutConstraints)
                    layoutConstraints.alignParentEnd -> measureXAlignParentEnd(layoutConstraints)
                    layoutConstraints.centerHorizontal || layoutConstraints.alignCenterInParent -> measureXAlignParentCenter()
                    else -> measureX(layoutConstraints)
                }
        )
        super.setY(
                when {
                    layoutConstraints.alignParentBottom -> measureYAlignParentBottom(layoutConstraints)
                    layoutConstraints.alignParentTop -> measureYAlignParentTop(layoutConstraints)
                    layoutConstraints.centerVertical || layoutConstraints.alignCenterInParent -> measureYAlignParentCenter()
                    else -> measureY(layoutConstraints)
                }
        )
        if (layoutConstraints.centerOrigin) {
            super.setOriginCenter()
        } else {
            super.setOrigin(
                    layoutConstraints.originX,
                    layoutConstraints.originY
            )
        }
    }

    fun measureXAlignParentStart(layoutConstraints: LayoutConstraints): Float = parent?.x?.plus(layoutConstraints.marginStart)
            ?: layoutConstraints.marginStart

    fun measureXAlignParentEnd(layoutConstraints: LayoutConstraints): Float {
        val p = parent
        return when {
            p != null -> p.x + p.width - layoutConstraints.marginEnd
            else -> world.width - layoutConstraints.marginEnd
        }
    }

    fun measureXAlignParentCenter(): Float {
        val p = parent
        return when {
            p != null -> p.x + p.width / 2 - width / 2
            else -> world.width / 2 - width / 2
        }
    }

    fun measureX(layoutConstraints: LayoutConstraints): Float {
        val p = parent
        return when {
            p != null && layoutConstraints.x == NOT_SET-> p.x + 0f + layoutConstraints.marginStart
            p != null -> p.x + layoutConstraints.x + layoutConstraints.marginStart
            layoutConstraints.x == NOT_SET -> 0f + layoutConstraints.marginStart
            else -> layoutConstraints.x + layoutConstraints.marginStart
        }
    }

    fun measureY(layoutConstraints: LayoutConstraints): Float {
        val p = parent
        return when {
            p != null && layoutConstraints.y == NOT_SET -> p.y + 0f + layoutConstraints.marginBottom
            p != null -> p.y + layoutConstraints.y + layoutConstraints.marginBottom
            layoutConstraints.y == NOT_SET -> 0f + layoutConstraints.marginBottom
            else -> layoutConstraints.y + layoutConstraints.marginBottom
        }
    }

    fun measureYAlignParentBottom(layoutConstraints: LayoutConstraints): Float = parent?.y?.plus(layoutConstraints.marginBottom)
            ?: layoutConstraints.marginBottom

    fun measureYAlignParentTop(layoutConstraints: LayoutConstraints): Float {
        val p = parent
        return when {
            p != null -> p.y + p.height - layoutConstraints.marginTop
            else -> world.height - layoutConstraints.marginTop
        }
    }

    fun measureYAlignParentCenter(): Float {
        val p = parent
        return when {
            p != null -> p.y + p.height / 2 - height / 2
            else -> world.height / 2 - height / 2
        }
    }

    fun measureWidthMatchParent(layoutConstraints: LayoutConstraints): Float {
        val p = parent
        return when {
            p != null -> p.width - layoutConstraints.marginStart - layoutConstraints.marginEnd
            else -> world.width - layoutConstraints.marginStart - layoutConstraints.marginEnd
        }
    }

    open fun measureWidthWrapContent(layoutConstraints: LayoutConstraints): Float {
        backgroundDrawable?.let {
            return it.width - layoutConstraints.marginStart - layoutConstraints.marginEnd
        }
        return 0f
    }

    fun measureHeightMatchParent(layoutConstraints: LayoutConstraints): Float {
        val p = parent
        return when {
            p != null -> p.height - layoutConstraints.marginBottom - layoutConstraints.marginTop
            else -> world.height - layoutConstraints.marginBottom - layoutConstraints.marginTop
        }
    }

    open fun measureHeightWrapContent(layoutConstraints: LayoutConstraints): Float {
        backgroundDrawable?.let {
            return it.height - layoutConstraints.marginBottom - layoutConstraints.marginTop
        }
        return 0f
    }
    // -----------------------------
    // endregion
    // -----------------------------

    // -----------------------------
    // region Drawing methods
    // -----------------------------
    open fun draw(batch: Batch, viewport: Viewport) {
        if (visibility != Visibility.VISIBLE) {
            return
        }
        /*
        Need to draw in the following order

        1. Background
        2. Draw view itself
        3. Draw any possible children
        4. Draw foreground
         */
        // 1. Draw background
        backgroundDrawable?.draw(batch = batch, x = x, y = y, width = width, height = height, originX = this.originX, originY = this.originY, scaleX = scaleX, scaleY = scaleY, rotation = rotation, sprite = this)

        // 2. Draw view itself
        onDraw(batch, viewport)

        // 3. Draw any possible children
        dispatchDraw(batch, viewport)

        // 4. Draw the foreground / view decorations
        drawForeground(batch, viewport)
    }

    open fun onDraw(batch: Batch, viewport: Viewport) {

    }

    open fun dispatchDraw(batch: Batch, viewport: Viewport) {

    }

    open fun drawForeground(batch: Batch, viewport: Viewport) {

    }
    // -----------------------------
    // endregion
    // -----------------------------

    // -----------------------------
    // region Input methods
    // -----------------------------
    fun dispatchTouchEvent(touchEvent: TouchEvent): Boolean {
        if (visibility == Visibility.GONE || !isTouching(touchEvent)) return false
        onTouch(this, touchEvent)
        if (focus) {
            if (clickable
                    && touchEvent.action === TouchEvent.Action.UP
                    && isTouching(touchEvent)) {
                onClick(this)
            }
        }
        if ((enabled || this is EditText) && focusable) {
            if (touchEvent.action === TouchEvent.Action.DOWN) {
                world.requestFocus(this)
            }
            onTouch(this, touchEvent)
            return onTouchEvent(touchEvent)
        }
        return false
    }

    open fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        return true
    }

    fun isTouching(touchEvent: TouchEvent): Boolean {
        return boundingRectangle.contains(touchEvent.x, touchEvent.y)
    }
    // -----------------------------
    // endregion
    // -----------------------------

    // -----------------------------
    // region Getters & Setters
    // -----------------------------
    open fun findViewById(id: Int): View? {
        return if (id == this.id) this else null
    }

    fun requestFocus(touchEvent: TouchEvent): View? {
        if (!focusable || visibility != Visibility.VISIBLE) {
            return null
        }
        if (!isTouching(touchEvent)) {
            return null
        }
        focus = true
        return this
    }

    fun clearFocus() {
        focus = false
    }

    override fun setSize(width: Float, height: Float) {
        layoutConstraints.width = width
        layoutConstraints.height = height
        invalidate()
    }

    override fun setWidth(width: Float) {
        layoutConstraints.width = width
        invalidate()
    }

    override fun setHeight(height: Float) {
        layoutConstraints.height = height
        invalidate()
    }

    override fun setX(x: Float) {
        layoutConstraints.x = x
        invalidate()
    }

    override fun setY(y: Float) {
        layoutConstraints.y = y
        invalidate()
    }

    override fun setPosition(x: Float, y: Float) {
        layoutConstraints.x = x
        layoutConstraints.y = y
        invalidate()
    }

    open fun invalidate() {
        dirty = true
    }


    override fun setColor(color: Float) {
        super.setColor(color)
        backgroundDrawable?.color = Color(NumberUtils.floatToIntColor(color))
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        backgroundDrawable?.color = Color(r, g, b, a)
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        backgroundDrawable?.color = tint
    }

    override fun setOriginBasedPosition(x: Float, y: Float) {
        super.setOriginBasedPosition(x, y)
    }

    open fun hideView(animation: Animation) {
        animation.animator.sprite = this
        animation.animator.start()
    }
    // -----------------------------
    // endregion
    // -----------------------------

    // -----------------------------
    // region Listeners
    // -----------------------------
    companion object {

        protected const val VERTEX_SIZE = 2 + 1 + 2
        protected const val VIEW_SIZE = 4 * VERTEX_SIZE

        val MATCH_PARENT = LayoutConstraints.MATCH_PARENT
        val WRAP_CONTENT = LayoutConstraints.WRAP_CONTENT
    }

    // -----------------------------
// endregion
    // -----------------------------
}

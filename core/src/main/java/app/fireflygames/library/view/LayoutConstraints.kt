package app.fireflygames.library.view

/**
 * Created by kodyvanry on 5/1/17.
 */

open class LayoutConstraints(var x: Float = NOT_SET,
                             var y: Float = NOT_SET,
                             var width: Float = SIZE_NOT_SET,
                             var height: Float = SIZE_NOT_SET,
                             margin: Float = 0f,
                             var marginStart: Float = 0f,
                             var marginEnd: Float = 0f,
                             var marginTop: Float = 0f,
                             var marginBottom: Float = 0f,
                             var alignParentStart: Boolean = false,
                             var alignParentEnd: Boolean = false,
                             var alignParentTop: Boolean = false,
                             var alignParentBottom: Boolean = false,
                             var centerVertical: Boolean = false,
                             var centerHorizontal: Boolean = false,
                             var alignCenterInParent: Boolean = false,
                             var originX: Float = 0f,
                             var originY: Float = 0f,
                             var centerOrigin: Boolean = true) {

    init {
        if (margin != 0f) {
            marginStart = margin
            marginEnd = margin
            marginTop = margin
            marginBottom = margin
        }
    }

    companion object {
        val WRAP_CONTENT = -1f
        val MATCH_PARENT = -2f

        val CENTER_HORIZONTAL = -1f
        val CENTER_VERTICAL = -2f
        val NOT_SET = -3f
        val SIZE_NOT_SET = 0f
    }

    override fun equals(other: Any?): Boolean {
        return other is LayoutConstraints &&
                other.marginStart == marginStart &&
                other.marginEnd == marginEnd &&
                other.marginTop == marginTop &&
                other.marginBottom == marginBottom &&
                other.alignParentStart == alignParentStart &&
                other.alignParentEnd == alignParentEnd &&
                other.alignParentTop == alignParentTop &&
                other.alignParentBottom == alignParentBottom &&
                other.x == x &&
                other.y == y &&
                other.width == width &&
                other.height == height
    }

    open fun clone(): LayoutConstraints {
        return LayoutConstraints(
                x, y, width, height, 0f, marginStart, marginEnd, marginTop, marginBottom, alignParentStart, alignParentEnd, alignParentTop, alignParentBottom, alignCenterInParent
        )
    }
}

package app.fireflygames.library.view

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.viewport.Viewport
import app.fireflygames.library.drawing.Drawable
import app.fireflygames.library.game.World

/**
 * Created by kodyvanry on 5/1/17.
 */

open class ProgressBar @JvmOverloads constructor(world: World, layoutConstraints: LayoutConstraints? = null) : View(world, layoutConstraints) {

    var progress: Float = 0f
        set(value) {
            field = Math.max(Math.min(value, 1f), 0f)
        }
    var progressBarHeight: Float = 0f
    var progressDrawable: Drawable? = null
    var progressBackgroundDrawable: Drawable? = null
    var verticalInset: Float = 0f
    var horizontalInset: Float = 0f
    var inset: Float
        set(value) {
            horizontalInset = value
            verticalInset = value
        }
        get() = verticalInset

    init {
        this.progressBarHeight = DEFAULT_PROGRESS_BAR_HEIGHT
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
        progressBackgroundDrawable?.draw(
                batch,
                x,
                y + height / 2 - progressBarHeight / 2,
                width,
                progressBarHeight
        )
        progressDrawable?.draw(
                batch,
                x + horizontalInset,
                y + height / 2 - progressBarHeight / 2 + verticalInset,
                (width - horizontalInset * 2) * progress,
                progressBarHeight - verticalInset * 2
        )
    }

    companion object {

        @JvmStatic protected val DEFAULT_PROGRESS_BAR_HEIGHT = 50f
    }
}

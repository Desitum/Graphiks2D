package app.fireflygames.library.view

import app.fireflygames.library.game.World
import app.fireflygames.library.game_objects.Visibility
import com.badlogic.gdx.graphics.Color
import java.util.*

/**
 * Created by kodyvanry on 5/1/17.
 */
@Suppress("UNNECESSARY_SAFE_CALL", "UNUSED")
open class ViewGroup @JvmOverloads constructor(world: World, layoutConstraints: LayoutConstraints? = null) : View(world, layoutConstraints) {

    var children: ArrayList<View> = ArrayList()
    override var z: Int
        get() = super.z
        set(value) {
            super.z = value
            children?.forEach {
                it.z = value + 1
            }
        }

    override var visibility: Visibility
        get() = super.visibility
        set(value) {
            super.visibility = value
            children.forEach { it.visibility = value }
        }

    open fun addView(v: View) {
        children.add(v)
        invalidate()
        v.z = z + 1
        v.parent = this
        world?.addView(v)
    }

    open fun removeView(v: View) {
        children.remove(v)
        world?.removeView(v)
    }

    override fun invalidate() {
        super.invalidate()
        children.forEach { it.invalidate() }
    }

    override fun setColor(color: Float) {
        super.setColor(color)
        children?.forEach { it.setColor(color) }
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        children?.forEach { it.setColor(tint) }
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        children?.forEach { it.setColor(r, g, b, a) }
    }

    fun hideViews(vararg ids: Int) {
        ids.forEach {
            findViewById(it)?.let { it.visibility = Visibility.GONE }
        }
    }

    override fun findViewById(id: Int): View? {
        children.forEach {
            it.findViewById(id)?.let { return it }
        }
        return super.findViewById(id)
    }
}

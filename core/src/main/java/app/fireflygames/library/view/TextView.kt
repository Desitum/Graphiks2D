package app.fireflygames.library.view

import app.fireflygames.library.extensions.setAlpha
import app.fireflygames.library.game.World
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.Viewport

/**
 * TextView.kt
 * IcebergEscape
 *
 * Created by Kody Van Ry on 6/17/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
@Suppress("Unused", "PLUGIN_WARNING", "MemberVisibilityCanBePrivate")
class TextView constructor(world: World, layoutConstraints: LayoutConstraints?, private var font: BitmapFont) : View(world, layoutConstraints) {

    private var dirty = true

    var text: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        set(value) {
            field = value
            invalidateText()
        }

    var alignment: Int = Align.center
        set(value) {
            field = value
            invalidateText()
        }

    private val glyphLayout: GlyphLayout
    var textColor: Color = Color.BLACK
        set(value) {
            field = value
            invalidateText()
        }

    var fontSize: Float = 150f
        set(value) {
            field = value
            invalidateText()
        }

    init {
        glyphLayout = GlyphLayout(font, text)
        text = ""
    }

    override fun onDraw(batch: Batch, viewport: Viewport) {
        super.onDraw(batch, viewport)
        font.data.setScale(fontSize / FONT_SIZE)
        font.draw(batch, glyphLayout, x, y + height / 2 + glyphLayout.height / 2)
    }

    fun updateGlyph() {
        textColor = textColor.setAlpha(this.color.a)
        font.data.setScale(fontSize / FONT_SIZE)
        glyphLayout.setText(font, text, textColor, width, alignment, true)
    }

    override fun update(delta: Float) {
        super.update(delta)
        if (dirty)
            updateGlyph()
    }

    private fun invalidateText() {
        dirty = true
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        invalidateText()
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        invalidateText()
    }

    override fun setColor(color: Float) {
        super.setColor(color)
        invalidateText()
    }

    companion object {
        private const val FONT_SIZE = 150f
    }
}

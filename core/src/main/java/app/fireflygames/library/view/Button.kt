package app.fireflygames.library.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.utils.NumberUtils
import app.fireflygames.library.drawing.Drawable
import app.fireflygames.library.game.AssetManager
import app.fireflygames.library.game.World

/**
 * Created by kodyvanry on 5/1/17.
 */

open class Button @JvmOverloads constructor(world: World, restDrawable: Drawable? = null, layoutConstraints: LayoutConstraints? = null) : View(world, layoutConstraints) {

    // DRAWING
    var restDrawable: Drawable? = restDrawable
        set(value) {
            backgroundDrawable = backgroundDrawable ?: value
            field = value
        }

    var hoverDrawable: Drawable? = null
        set(value) {
            backgroundDrawable = backgroundDrawable ?: value
            field = value
        }

    override fun setColor(color: Float) {
        super.setColor(color)
        restDrawable?.color = Color(NumberUtils.floatToIntColor(color))
        hoverDrawable?.color = Color(NumberUtils.floatToIntColor(color))
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        super.setColor(r, g, b, a)
        restDrawable?.color = Color(r, g, b, a)
        hoverDrawable?.color = Color(r, g, b, a)
    }

    override fun setColor(tint: Color) {
        super.setColor(tint)
        restDrawable?.color = tint
        hoverDrawable?.color = tint
    }

    init {
        clickable = true
        backgroundDrawable = restDrawable
    }

    override fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        when (touchEvent.action) {
            TouchEvent.Action.DOWN -> if (hoverDrawable != null)
                backgroundDrawable = hoverDrawable
            TouchEvent.Action.MOVE -> if (isTouching(touchEvent) && hoverDrawable != null)
                backgroundDrawable = hoverDrawable
            else -> backgroundDrawable = restDrawable
        }
        return super.onTouchEvent(touchEvent)
    }
}

package app.fireflygames.library.view

import app.fireflygames.library.game.World

/**
 * Created by kodyvanry on 5/9/17.
 */

open class LinearLayout @JvmOverloads constructor(world: World, orientation: Orientation = Orientation.VERTICAL, layoutConstraints: LayoutConstraints? = null) : ViewGroup(world, layoutConstraints) {

    enum class Orientation {
        HORIZONTAL,
        VERTICAL
    }

    var orientation = orientation
        set(value) {
            field = value
            invalidate()
        }

    override fun addView(v: View) {
        super.addView(v)
        v.layoutConstraints = getNewLayoutConstraints(v)
    }

    override fun measureWidthWrapContent(layoutConstraints: LayoutConstraints): Float {
        return children.map { it.width + it.layoutConstraints.marginStart + it.layoutConstraints.marginEnd }.sum()
    }

    override fun measureHeightWrapContent(layoutConstraints: LayoutConstraints): Float {
        return children.map { it.height + it.layoutConstraints.marginTop + it.layoutConstraints.marginBottom }.sum()
    }

    open protected fun getNewLayoutConstraints(v: View): LayoutConstraints {
        val layoutConstraints: LayoutConstraints = v.layoutConstraints

        if (orientation == Orientation.HORIZONTAL) {
            if (layoutConstraints.width == LayoutConstraints.MATCH_PARENT)
                throw RuntimeException(v.toString() + " : " + v.name + " - Parent orientation is HORIZONTAL. View cannot have width of MATCH_PARENT")
            if (layoutConstraints.width == 0f)
                layoutConstraints.width = v.width
            if (layoutConstraints.height == 0f)
                layoutConstraints.height = LayoutConstraints.MATCH_PARENT
            if (layoutConstraints.x == LayoutConstraints.CENTER_HORIZONTAL)
                throw RuntimeException(v.toString() + " : " + v.name + " - Parent orientation is HORIZONTAL. View cannot center horizontally")
            else
                layoutConstraints.x = getNewX(v)
            layoutConstraints.y = 0f
        } else if (orientation == Orientation.VERTICAL) {
            if (layoutConstraints.height == LayoutConstraints.MATCH_PARENT)
                throw RuntimeException(v.toString() + " : " + v.name + " - Parent orientation is VERTICAL. View cannot have height of MATCH_PARENT")
            if (layoutConstraints.height == 0f)
                layoutConstraints.height = v.height
            if (layoutConstraints.width == 0f)
                layoutConstraints.width = LayoutConstraints.MATCH_PARENT
            if (layoutConstraints.y == LayoutConstraints.CENTER_VERTICAL)
                throw RuntimeException(v.toString() + " : " + v.name + " - Parent orientation is VERTICAL. View cannot center vertically")
            else
                layoutConstraints.y = getNewY(v)
        }
        return layoutConstraints
    }

    override fun dispatchLayout() {
        super.dispatchLayout()
        children.forEach { it.layoutConstraints = getNewLayoutConstraints(it) }
    }

    private fun getNewX(child: View): Float {
        return children.subList(0, children.indexOf(child)).map { it.layoutConstraints.width + it.layoutConstraints.marginStart + it.layoutConstraints.marginEnd }.sum()
    }

    private fun getNewY(child: View): Float {
        return height - children.subList(0, children.indexOf(child) + 1).map { it.layoutConstraints.height + it.layoutConstraints.marginTop + it.layoutConstraints.marginBottom }.sum() + layoutConstraints.height
    }

    companion object {
        val ALIGNMENT_TOP = 0
        val ALIGNMENT_BOTTOM = 1
        val ALIGNMENT_CENTER = 2
        val ALIGNMENT_RIGHT = 1
        val ALIGNMENT_LEFT = 0
    }
}

package app.fireflygames.library.animation

import com.badlogic.gdx.graphics.g2d.Sprite
import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.AccelerateDecelerateInterpolator
import app.fireflygames.library.interpolation.AccelerateInterpolator
import app.fireflygames.library.interpolation.AnticipateInterpolator
import app.fireflygames.library.interpolation.BounceInterpolator
import app.fireflygames.library.interpolation.DecelerateInterpolator
import app.fireflygames.library.interpolation.Interpolation
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator
import app.fireflygames.library.interpolation.OvershootInterpolator
import app.fireflygames.library.view.LayoutConstraints
import app.fireflygames.library.view.View

/**
 * Created by kody on 2/24/15.
 * can be used by kody and people in []
 */
class MarginAnimator @JvmOverloads constructor(
        sprite: G2DSprite?,
        startMargins: LayoutConstraints,
        endMargins: LayoutConstraints,
        duration: Float,
        delay: Float = 0f,
        interpolator: Interpolator = LinearInterpolator.get()
        ) : Animator(sprite, duration, delay, interpolator) {

    var startMargins: LayoutConstraints = startMargins
        private set
    var endMargins: LayoutConstraints = endMargins
        private set
    var currentMargins: LayoutConstraints = startMargins.clone()
        private set

    var distanceMarginStart = endMargins.marginStart - startMargins.marginStart
    var distanceMarginEnd = endMargins.marginEnd - startMargins.marginEnd
    var distanceMarginTop = endMargins.marginTop - startMargins.marginTop
    var distanceMarginBottom = endMargins.marginBottom - startMargins.marginBottom

    override fun reset() {
        super.reset()
        currentMargins = startMargins.clone()
    }

    override fun start() {
        super.start()
        if (sprite is View)
            (sprite as View).layoutConstraints = currentMargins
    }

    override fun updateAnimation() {
        currentMargins.marginStart = interpolator.getInterpolation(timeInAnimation) * distanceMarginStart + startMargins.marginStart
        currentMargins.marginEnd = interpolator.getInterpolation(timeInAnimation) * distanceMarginEnd + startMargins.marginEnd
        currentMargins.marginTop = interpolator.getInterpolation(timeInAnimation) * distanceMarginTop + startMargins.marginTop
        currentMargins.marginBottom = interpolator.getInterpolation(timeInAnimation) * distanceMarginBottom + startMargins.marginBottom
//        if (sprite is View)
//            (sprite as View).layoutConstraints = currentMargins
    }

    override fun clone(): Animator {
        return MarginAnimator(this.sprite!!, this.startMargins, this.endMargins, duration, delay, interpolator.copy())
    }
}

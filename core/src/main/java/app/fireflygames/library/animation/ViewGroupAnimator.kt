package app.fireflygames.library.animation

import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator
import app.fireflygames.library.view.LayoutConstraints
import app.fireflygames.library.view.View
import app.fireflygames.library.view.ViewGroup

/**
 * Created by kodyvanry on 7/5/17
 */
@Suppress("Unused", "PLUGIN_WARNING", "MemberVisibilityCanBePrivate")
class ViewGroupAnimator(val viewGroup: ViewGroup) {

    val animators: ArrayList<Animator> = ArrayList()

    private fun animateTo(view: View, newLayout: LayoutConstraints, duration: Float, delay: Float=0f, interpolation: Interpolator = LinearInterpolator.get()) {
        val fromLayoutConstraints = view.layoutConstraints.clone()
        val layoutConstraints = view.layoutConstraints.clone()

        getHorizontalAlignment(view, newLayout, fromLayoutConstraints, layoutConstraints)
        getVerticalAlignment(view, newLayout, fromLayoutConstraints, layoutConstraints)

        getSizeX(view, newLayout, fromLayoutConstraints, layoutConstraints)
        getSizeY(view, newLayout, fromLayoutConstraints, layoutConstraints)

        val marginAnimator = MarginAnimator(view, fromLayoutConstraints, layoutConstraints, duration, delay, interpolation)
        val sizeAnimator = SizeAnimator(view, duration, delay, fromLayoutConstraints.width, layoutConstraints.width, fromLayoutConstraints.height, layoutConstraints.height, interpolation)
        view.addAnimation(marginAnimator)
        view.addAnimation(sizeAnimator)
        animators.add(marginAnimator)
        animators.add(sizeAnimator)
    }

    @JvmOverloads fun animateView(id: Int, newLayout: LayoutConstraints, duration: Float, delay: Float=0f, interpolation: Interpolator = LinearInterpolator.get()) {
        animateTo(viewGroup.findViewById(id)!!, newLayout, duration, delay, interpolation)
    }

    @JvmOverloads fun addViewToAnimate(id: Int, view: View, newLayout: LayoutConstraints, duration: Float, delay: Float=0f, interpolation: Interpolator = LinearInterpolator.get()) {
        viewGroup.findViewById(id)?.layoutConstraints = view.layoutConstraints // TODO this is not right at all
        animateView(id, newLayout, duration, delay, interpolation)
    }

    fun getHorizontalAlignment(view: View, newLayout: LayoutConstraints, fromLayoutConstraints: LayoutConstraints, layoutConstraints: LayoutConstraints) : Array<LayoutConstraints> {
        if (newLayout.alignParentStart != view.layoutConstraints.alignParentStart || newLayout.alignParentEnd != view.layoutConstraints.alignParentEnd) {
            layoutConstraints.alignParentStart = newLayout.alignParentStart
            layoutConstraints.alignParentEnd = newLayout.alignParentEnd
            if (newLayout.alignParentStart && view.layoutConstraints.alignParentEnd) {
                layoutConstraints.marginStart = newLayout.marginStart
                fromLayoutConstraints.marginStart = view.parent?.width?.minus(view.layoutConstraints.marginEnd)?.minus(view.width) ?: 0f
            }
            if (newLayout.alignParentEnd && view.layoutConstraints.alignParentStart) {
                layoutConstraints.marginEnd = newLayout.marginEnd
                fromLayoutConstraints.marginEnd = view.layoutConstraints.marginStart
            }
        } else if (newLayout.marginStart != view.layoutConstraints.marginStart) {
            layoutConstraints.marginStart = newLayout.marginStart
            fromLayoutConstraints.marginStart = view.layoutConstraints.marginStart
        } else if (newLayout.marginEnd != view.layoutConstraints.marginEnd) {
            layoutConstraints.marginEnd = newLayout.marginEnd
            fromLayoutConstraints.marginEnd = view.layoutConstraints.marginEnd
        }

        return arrayOf(fromLayoutConstraints, layoutConstraints)
    }

    fun getVerticalAlignment(view: View, newLayout: LayoutConstraints, fromLayoutConstraints: LayoutConstraints, layoutConstraints: LayoutConstraints) : Array<LayoutConstraints> {
        if (newLayout.alignParentBottom != view.layoutConstraints.alignParentBottom || newLayout.alignParentTop != view.layoutConstraints.alignParentTop) {
            layoutConstraints.alignParentBottom = newLayout.alignParentBottom
            layoutConstraints.alignParentTop = newLayout.alignParentTop
            if (newLayout.alignParentBottom && view.layoutConstraints.alignParentTop) {
                layoutConstraints.marginBottom = newLayout.marginBottom
                fromLayoutConstraints.marginBottom = view.parent?.height?.minus(view.layoutConstraints.marginTop)?.minus(view.height) ?: 0f
            }
            if (newLayout.alignParentTop && view.layoutConstraints.alignParentBottom) {
                layoutConstraints.marginTop = newLayout.marginTop
                fromLayoutConstraints.marginTop = view.layoutConstraints.marginBottom
            }
        } else if (newLayout.marginBottom != view.layoutConstraints.marginBottom) {
            layoutConstraints.marginBottom = newLayout.marginBottom
            fromLayoutConstraints.marginBottom = view.layoutConstraints.marginBottom
        } else if (newLayout.marginTop != view.layoutConstraints.marginTop) {
            layoutConstraints.marginTop = newLayout.marginTop
            fromLayoutConstraints.marginTop = view.layoutConstraints.marginTop
        }

        return arrayOf(fromLayoutConstraints, layoutConstraints)
    }

    fun getSizeX(view: View, newLayout: LayoutConstraints, fromLayoutConstraints: LayoutConstraints, layoutConstraints: LayoutConstraints) : Float {
        if (newLayout.width != view.layoutConstraints.width) {
            fromLayoutConstraints.width = view.layoutConstraints.width
            layoutConstraints.width = newLayout.width
            return newLayout.width / view.layoutConstraints.width
        }
        return view.layoutConstraints.width
    }

    fun getSizeY(view: View, newLayout: LayoutConstraints, fromLayoutConstraints: LayoutConstraints, layoutConstraints: LayoutConstraints) : Float {
        if (newLayout.height != view.layoutConstraints.height) {
            fromLayoutConstraints.height = view.layoutConstraints.height
            layoutConstraints.height = newLayout.height
            return newLayout.height / view.layoutConstraints.height
        }
        return view.layoutConstraints.height
    }

    fun start() {
        animators.forEach { it.start() }
    }

}
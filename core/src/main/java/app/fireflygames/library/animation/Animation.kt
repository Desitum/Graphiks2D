package app.fireflygames.library.animation

import app.fireflygames.library.interpolation.AccelerateInterpolator
import app.fireflygames.library.interpolation.Interpolator
import com.badlogic.gdx.graphics.Color

/**
 * Created by kodyvanry on 6/29/17.
 */

enum class Animation constructor(var animator: Animator) {
    FADE_AWAY(ColorEffects(null, Interpolator.ANIMATION_LONG, 0f, Color.WHITE, Color(1f, 1f, 1f, 0f))),
    FADE_AWAY_FAST(ColorEffects(null, Interpolator.ANIMATION_SHORT, 0f, Color.WHITE, Color(1f, 1f, 1f, 0f))),
    FADE_IN(ColorEffects(null, Interpolator.ANIMATION_LONG, 0f, Color(1f, 1f, 1f, 0f), Color.WHITE)),
    FADE_IN_FAST(ColorEffects(null, Interpolator.ANIMATION_SHORT, 0f, Color(1f, 1f, 1f, 0f), Color.WHITE)),
    SLIDE_OUT_BOTTOM(MovementAnimator(null, Interpolator.ANIMATION_SHORT, 0f, 0f, 0f, 0f, 0f, AccelerateInterpolator.get())),
    SLIDE_OUT_TOP(MovementAnimator(null, Interpolator.ANIMATION_SHORT, 0f, 0f, 0f, 0f, 0f, AccelerateInterpolator.get())),
    SLIDE_OUT_RIGHT(MovementAnimator(null, Interpolator.ANIMATION_SHORT, 0f, 0f, 0f, 0f, 0f, AccelerateInterpolator.get())),
    SLIDE_OUT_LEFT(MovementAnimator(null, Interpolator.ANIMATION_SHORT, 0f, 0f, 0f, 0f, 0f, AccelerateInterpolator.get())),
    SHRINK_AWAY(ScaleAnimator(null, Interpolator.ANIMATION_SHORT, 0f, 1f, 0f, AccelerateInterpolator.get(), true, true)),
    GROW_FROM_NOTHING(ScaleAnimator(null, Interpolator.ANIMATION_SHORT, 0f, 0f, 1f, AccelerateInterpolator.get(), true, true)),
}
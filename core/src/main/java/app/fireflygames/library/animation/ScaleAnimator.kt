package app.fireflygames.library.animation

import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator

/**
 * Created by dvan6234 on 2/24/2015.
 */
class ScaleAnimator(sprite: G2DSprite?, duration: Float, delay: Float, private val startScale: Float, private val endScale: Float, interpolator: Interpolator = LinearInterpolator.get(), controlWidth: Boolean = true, controlHeight: Boolean = true) : Animator(sprite, duration, delay, interpolator) {

    var scaleSize: Float = 0f
        private set

    private var growing: Boolean = false

    var controllingX: Boolean = controlWidth
        private set
        @JvmName("isControllingX") get

    var controllingY: Boolean = controlHeight
        private set
        @JvmName("isControllingY") get

    override fun updateAnimation() {
        if (growing) {
            scaleSize = startScale + (endScale - startScale) * interpolator.getInterpolation(timeInAnimation)
        } else {
            scaleSize = startScale - (startScale - endScale) * interpolator.getInterpolation(timeInAnimation)
        }

        if (controllingY) {
            sprite?.setScale(sprite!!.scaleX, this.scaleSize)
        }
        if (controllingX) {
            sprite?.setScale(this.scaleSize, sprite!!.scaleY)
        }
    }

    override fun clone(): Animator {
        return ScaleAnimator(sprite, duration, delay, startScale, endScale, interpolator, controllingX, controllingY)
    }

    init {
        growing = startScale <= endScale
    }
}
// 150
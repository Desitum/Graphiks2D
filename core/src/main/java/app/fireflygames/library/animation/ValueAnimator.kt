package app.fireflygames.library.animation

import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator

/**
 * Created by kodyvanry on 8/1/17
 */
class ValueAnimator(duration: Float, val valueFrom: Float, val valueTo: Float, delay: Float, interpolater: Interpolator = LinearInterpolator.get()) : Animator(null, duration, delay, interpolater) {

    val listeners = ArrayList<(Animator) -> Unit>()
    var distanceValue: Float = valueTo - valueFrom
    var animatedValue: Float = valueFrom

    override fun clone(): Animator {
        return ValueAnimator(duration, valueFrom, valueTo, delay, interpolator.copy())
    }

    override fun updateAnimation() {
        animatedValue = interpolator.getInterpolation(timeInAnimation) * distanceValue + valueFrom
        listeners.forEach { it(this) }
    }

    fun addListener(listener: (Animator) -> Unit) {
        listeners.add(listener)
    }

    init {
        this.distanceValue = valueTo - valueFrom
    }

}

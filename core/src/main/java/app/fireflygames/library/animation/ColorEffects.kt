package app.fireflygames.library.animation

import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator
import com.badlogic.gdx.graphics.Color

/**
 * Created by dvan6234 on 2/17/2015.
 */
@Suppress("Unused", "PLUGIN_WARNING", "MemberVisibilityCanBePrivate")
class ColorEffects @JvmOverloads constructor(sprite: G2DSprite?, duration: Float, delay: Float = 0f, startColor: Color, endColor: Color, interpolator: Interpolator = LinearInterpolator.get()) : Animator(sprite, duration, delay, interpolator) {
    private var startRed: Float = 0f
    private var startGreen: Float = 0f
    private var startBlue: Float = 0f
    private var startAlpha: Float = 0f

    private var slopeRed: Float = 0f
    private var slopeGreen: Float = 0f
    private var slopeBlue: Float = 0f
    private var slopeAlpha: Float = 0f

    private var endRed: Float = 0f
    private var endGreen: Float = 0f
    private var endBlue: Float = 0f
    private var endAlpha: Float = 0f

    var currentRed: Float = 0f
        private set
    var currentGreen: Float = 0f
        private set
    var currentBlue: Float = 0f
        private set
    var currentAlpha: Float = 0f
        private set

    init {
        setupColors(startColor, endColor, duration)
    }

    private fun setupColors(startColor: Color, endColor: Color, duration: Float) {
        if (duration <= 0) {
            currentRed = endColor.r
            endRed = endColor.r
            slopeRed = 0f
            currentGreen = endColor.r
            endGreen = endColor.g
            slopeGreen = 0f
            currentBlue = endColor.b
            endBlue = endColor.b
            slopeBlue = 0f
            currentAlpha = endColor.a
            endAlpha = endColor.a
            slopeAlpha = 0f
            return
        }

        startRed = startColor.r
        startGreen = startColor.g
        startBlue = startColor.b
        startAlpha = startColor.a

        slopeRed = endColor.r - startColor.r
        slopeGreen = endColor.g - startColor.g
        slopeBlue = endColor.b - startColor.b
        slopeAlpha = endColor.a - startColor.a

        currentRed = startColor.r
        currentGreen = startColor.g
        currentBlue = startColor.b
        currentAlpha = startColor.a

        endRed = endColor.r
        endGreen = endColor.g
        endBlue = endColor.b
        endAlpha = endColor.a
    }

    val currentColor: Color
        get() = Color(currentRed, currentGreen, currentBlue, currentAlpha)

    /**
     * Duplicates this ColorEffects instance
     * @return New instance of ColorEffects
     */
    override fun clone(): Animator {
        return ColorEffects(sprite, duration, delay, Color(startRed, startGreen, startBlue, startAlpha), Color(endRed, endGreen, endBlue, endAlpha))
    }

    override fun updateAnimation() {
        currentRed = slopeRed * interpolator.getInterpolation(timeInAnimation) + startRed
        currentGreen = slopeGreen * interpolator.getInterpolation(timeInAnimation) + startGreen
        currentBlue = slopeBlue * interpolator.getInterpolation(timeInAnimation) + startBlue
        currentAlpha = slopeAlpha * interpolator.getInterpolation(timeInAnimation) + startAlpha

        sprite?.color = currentColor
    }

    override fun equals(other: Any?): Boolean {
        return other is ColorEffects &&
                other.duration == duration &&
                other.delay == delay &&
                other.startAlpha == startAlpha &&
                other.startRed == startRed &&
                other.startGreen == startGreen &&
                other.startBlue == startBlue &&
                other.endRed == endRed &&
                other.endGreen == endGreen &&
                other.endBlue == endBlue &&
                other.endAlpha == endAlpha
    }

    override fun hashCode(): Int {
        var result = startRed.hashCode()
        result = 31 * result + startGreen.hashCode()
        result = 31 * result + startBlue.hashCode()
        result = 31 * result + startAlpha.hashCode()
        result = 31 * result + endRed.hashCode()
        result = 31 * result + endGreen.hashCode()
        result = 31 * result + endBlue.hashCode()
        result = 31 * result + endAlpha.hashCode()
        return result
    }

    companion object {

        @JvmStatic fun colorsMatch(color1: Color, color2: Color, marginOfError: Float): Boolean {
            if (color1 == color2) return true

            var error = 0f

            error += Math.abs(color1.r - color2.r)
            error += Math.abs(color1.g - color2.g)
            error += Math.abs(color1.b - color2.b)
            error += Math.abs(color1.a - color2.a)

            return error < marginOfError
        }
    }
}


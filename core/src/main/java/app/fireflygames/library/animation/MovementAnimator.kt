package app.fireflygames.library.animation

import app.fireflygames.library.extensions.centerX
import app.fireflygames.library.extensions.centerY
import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator

/**
 * Created by kody on 2/24/15.
 * can be used by kody and people in []
 */
class MovementAnimator(sprite: G2DSprite?, duration: Float, delay: Float, private val startX: Float, private val endX: Float, private val startY: Float, private val endY: Float, interpolator: Interpolator = LinearInterpolator.get(), private val useCenter: Boolean = false) : Animator(sprite, duration, delay, interpolator) {

    var distanceX: Float = endX - startX
        private set

    var distanceY: Float = endY - startY
        private set

    override fun updateAnimation() {
        if (useCenter) {
            if (distanceX != 0f)
                sprite?.centerX = interpolator.getInterpolation(timeInAnimation) * distanceX + startX
            if (distanceY != 0f)
                sprite?.centerY = interpolator.getInterpolation(timeInAnimation) * distanceY + startY
        } else {
            if (distanceX != 0f)
                sprite?.x = interpolator.getInterpolation(timeInAnimation) * distanceX + startX
            if (distanceY != 0f)
                sprite?.y = interpolator.getInterpolation(timeInAnimation) * distanceY + startY
        }
    }

    override fun clone(): Animator {
        return MovementAnimator(sprite, duration, delay, startX, endX, startY, endY, interpolator.copy())
    }
}

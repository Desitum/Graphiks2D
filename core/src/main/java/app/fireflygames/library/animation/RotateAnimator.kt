package app.fireflygames.library.animation

import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator

/**
 * Created by kody on 12/21/15.
 * can be used by kody and people in [kody}]
 */
class RotateAnimator(sprite: G2DSprite?, duration: Float, delay: Float, private val startRotation: Float, private val endRotation: Float, interpolator: Interpolator = LinearInterpolator.get()) : Animator(sprite, duration, delay, interpolator) {

    private var amountToRotate: Float = endRotation - startRotation

    override fun updateAnimation() {
        sprite?.rotation = interpolator.getInterpolation(timeInAnimation) * amountToRotate + startRotation
    }

    override fun clone(): Animator {
        return RotateAnimator(sprite, duration, delay, startRotation, endRotation, interpolator.copy())
    }
}
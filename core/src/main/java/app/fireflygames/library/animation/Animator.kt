@file:JvmName("Animator")

package app.fireflygames.library.animation

import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.*

/**
 * Created by kody on 2/24/2015
 */
abstract class Animator(var sprite: G2DSprite?, var duration: Float, var delay: Float, var interpolator: Interpolator) {

    var timeInAnimation: Float = 0f
    var currentDelay: Float = 0f
    var isRunning: Boolean = false
    var isRan: Boolean = false
    var onAnimationFinished: (Animator) -> Unit = {}
    var onAnimationUpdated: (Animator) -> Unit = {}
    var onAnimationRepeat: (Animator) -> Unit = {}
    val totalTime: Float
        get() = delay + duration
    var repeatCount: Int = 0

    open fun update(delta: Float) {
        if (!isRunning) {
            return
        }
        currentDelay += delta
        if (currentDelay >= delay) {
            timeInAnimation += delta / duration
        }
        if (timeInAnimation >= 1) {
            if (repeatCount == INFINITE || repeatCount > 0) {
                repeatCount = Math.max(INFINITE, repeatCount - 1)
                timeInAnimation = 0f
                onAnimationRepeat(this)
            } else {
                timeInAnimation = 1f
                isRunning = false
                isRan = true
                onAnimationFinished(this)
            }
        }
        updateAnimation()
        onAnimationUpdated(this)
    }

    open fun start() {
        reset()
        isRunning = true
        updateAnimation()
    }

    open fun reset() {
        this.isRunning = false
        this.timeInAnimation = 0f
        this.currentDelay = 0f
    }

    abstract fun clone(): Animator

    fun didFinish(): Boolean {
        return isRan && !isRunning
    }

    abstract fun updateAnimation()
    //endregion

    companion object {
        const val INFINITE = -1
    }
}

package app.fireflygames.library.animation

import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.interpolation.LinearInterpolator

/**
 * Created by dvan6234 on 2/24/2015.
 */
class SizeAnimator(sprite: G2DSprite?, duration: Float, delay: Float, private val startWidth: Float, private val endWidth: Float, private val startHeight: Float, private val endHeight: Float, interpolator: Interpolator = LinearInterpolator.get()) : Animator(sprite, duration, delay, interpolator) {

    var widthChange: Float = 0f
        private set

    var heightChange: Float = 0f
        private set

    override fun updateAnimation() {
        if (widthChange != 0f)
            sprite?.width = startWidth + widthChange * interpolator.getInterpolation(timeInAnimation)
        if (heightChange != 0f)
            sprite?.height = startHeight + heightChange * interpolator.getInterpolation(timeInAnimation)
    }

    override fun clone(): Animator {
        return SizeAnimator(sprite, duration, delay, startWidth, endWidth, startHeight, endHeight, interpolator.copy())
    }

    init {
        widthChange = this.endWidth - this.startWidth
        heightChange = this.endHeight - this.startHeight
    }
}
package app.fireflygames.library.game_objects

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.viewport.Viewport
import app.fireflygames.library.animation.Animator
import app.fireflygames.library.animation.MovementAnimator
import app.fireflygames.library.drawing.Drawable
import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.game.World
import app.fireflygames.library.interpolation.Interpolator
import app.fireflygames.library.view.TouchEvent
import java.util.*

/**
 * Created by kody on 12/27/15.
 * can be used by kody and people in [kody}]
 */
open class GameObject(textureRegion: TextureRegion?, world: World?) : G2DSprite() {
    var animators: ArrayList<Animator>
        private set
    var onFinishedMovingListener: OnFinishedMovingListener? = null
    var visibility = Visibility.VISIBLE

    var speed: Float = 0f
    var speedX: Float = 0f
    var speedY: Float = 0f
    var gravityX: Float = 0f
    var gravityY: Float = 0f
    var rotationSpeed: Float = 0f
    var rotationResistance: Float = 0f
    var focus: Boolean = false
    var focusable = false
        @JvmName("isFocusable") get
    var drawable: Drawable? = null
    private var moveTo: FloatArray? = null
    private var world: World? = null
    var needsRemoval = false



    var gameObjects = ArrayList<GameObject>()
    private var gameObjectsToAdd = ArrayList<GameObject>()
    private var gameObjectsToRemove = ArrayList<GameObject>()

    init {
        this.world = world
        textureRegion?.let { setRegion(it) }
    }

    override fun update(delta: Float) {
        when (visibility) {
            Visibility.VISIBLE, Visibility.INVISIBLE -> {
                updateAnimators(delta)
                updatePosition(delta)
            }
            Visibility.GONE -> return // SKIP BECAUSE IT'S JUST GONE
        }
    }

    private fun updatePosition(delta: Float) {
        speedX += gravityX * delta
        speedY += gravityY * delta
        x += speedX * delta
        y += speedY * delta
        rotationSpeed *= rotationResistance
        rotation += rotationSpeed * delta
        if (moveTo != null) {
            updateMovement()
        }
    }

    private fun updateAnimators(delta: Float) {
        if (!animators.isEmpty()) {
            animators.forEach { it.update(delta) }
            animators.removeAll { it.didFinish() }
        }
    }

    private fun updateMovement() {
        if (speedX < 0) {
            if (x < moveTo!![0]) {
                moveFinished()
            }
        } else if (speedX > 0) {
            if (x > moveTo!![0]) {
                moveFinished()
            }
        }
        if (speedY < 0) {
            if (y < moveTo!![1]) {
                moveFinished()
            }
        } else if (speedY > 0) {
            if (y > moveTo!![1]) {
                moveFinished()
            }
        }
    }

    private fun moveFinished() {
        x = moveTo!![0]
        y = moveTo!![1]
        moveTo = null
        onFinishedMovingListener?.onFinished(this)
    }

    fun moveToPosition(x: Float, y: Float) {
        // x = cos
        // y = sin
        val deltaX = getX() - x
        val deltaY = getY() - y
        val angle = Math.atan2(deltaY.toDouble(), deltaX.toDouble()).toFloat() // in radians
        speedX = Math.cos(angle.toDouble()).toFloat() * speed
        speedY = Math.sin(angle.toDouble()).toFloat() * speed
        moveTo = floatArrayOf(x, y)
    }

    fun smoothMoveTo(x: Float, y: Float, duration: Float, interpolation: Interpolator) {
        val animator = MovementAnimator(this, duration, 0f, this.x, x, this.y, y, interpolation)
        animator.start()
        this.animators.add(animator)
    }

    fun addAndStartAnimator(anim: Animator) {
        anim.sprite = this
        animators.add(anim)
        anim.start()
    }

    fun addAnimator(anim: Animator) {
        anim.sprite = this
        animators.add(anim)
    }

    fun dispose() {
        try {
            texture.dispose()
        } catch (e: Exception) {
            // Texture has been disposed of elsewhere
        }
    }

    // -----------------------------
    // region Input methods
    // -----------------------------
    fun dispatchTouchEvent(touchEvent: TouchEvent): Boolean {
        if (touchEvent.action === TouchEvent.Action.DOWN && focusable) {
            world?.requestFocus(this)
        }
        return onTouchEvent(touchEvent)
    }

    open fun onTouchEvent(touchEvent: TouchEvent): Boolean {
        return true
    }

    fun isTouching(touchEvent: TouchEvent): Boolean {
        return boundingRectangle.contains(touchEvent.x, touchEvent.y)
    }
    // -----------------------------
    // endregion
    // -----------------------------

    // -----------------------------
    // region Drawing methods
    // -----------------------------
    fun draw(batch: Batch, viewport: Viewport) {
        if (visibility != Visibility.VISIBLE) return
        if (texture != null) draw(batch)
        /*
        Need to draw in the following order

        1. Background
        2. Draw Underneath View
        3. Draw view itself
        4. Draw any possible children
        5. Draw foreground
         */
        // 1. Draw the underside
        preDraw(batch, viewport)

        // 2. Draw background
        drawable?.draw(batch, x, y, width, height)

        // 3. Draw view itself
        onDraw(batch, viewport)

        // 4. Draw any possible children
        dispatchDraw(batch, viewport)

        // 5. Draw the foreground / view decorations
        drawForeground(batch, viewport)
    }

    open fun preDraw(spriteBatch: Batch, viewport: Viewport) {

    }

    open fun onDraw(spriteBatch: Batch, viewport: Viewport) {

    }

    open fun dispatchDraw(spriteBatch: Batch, viewport: Viewport) {

    }

    open fun drawForeground(spriteBatch: Batch, viewport: Viewport) {

    }
    // -----------------------------
    // endregion
    // -----------------------------

    fun clearFocus() {
        focus = false
    }

    companion object {
        const val DEFAULT_Z = 0
    }

    init {
        animators = arrayListOf()
        z = DEFAULT_Z
    }
}

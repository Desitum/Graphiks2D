package app.fireflygames.library.game_objects

/**
 * Created by kodyvanry on 7/18/17.
 */
enum class Visibility {
    VISIBLE,
    INVISIBLE,
    GONE
}
package app.fireflygames.library.extensions

/**
 * NumberExtensions.kt
 * Graphiks2D
 *
 * Created by kvanry on 6/1/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
val Double.float: Float
    get() { return toFloat() }

val Double.radians: Double
    get() = this * Math.PI / 180f

val Double.degrees: Double
    get() = this * 180f / Math.PI

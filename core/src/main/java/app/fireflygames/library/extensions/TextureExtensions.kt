package app.fireflygames.library.extensions

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * TextureExtensions
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/7/18.
 * Copyright (c) 2018. FireFly Labs. All rights reserved worldwide.
 */
fun Texture.toRegion(): TextureRegion {
    return TextureRegion(this)
}

val Texture.region: TextureRegion
    get() = TextureRegion(this)


val TextureRegion.flipped: TextureRegion
    get() {
        val newRegion = TextureRegion(this)
        newRegion.flip(true, false)
        return newRegion
    }

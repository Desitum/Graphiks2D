package app.fireflygames.library.extensions

import app.fireflygames.library.game.G2DSprite
import app.fireflygames.library.game_objects.GameObject
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2

/**
 * GameObjectExtensions.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/13/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */

//var GameObject.centerX: Float
//    get() = x + width / 2f
//    set(value) {
//        x = value - width / 2f
//    }
//
//var GameObject.centerY: Float
//    get() = y + height / 2f
//    set(value) {
//        y = value - height / 2f
//    }
//
//val GameObject.centerVector: Vector2
//    get() = Vector2(centerX, centerY)

var G2DSprite.centerX: Float
    get() = x + width / 2f
    set(value) {
        x = value - width / 2f
    }

var G2DSprite.centerY: Float
    get() = y + height / 2f
    set(value) {
        y = value - height / 2f
    }

val G2DSprite.centerVector: Vector2
    get() = Vector2(centerX, centerY)

fun Color.setAlpha(alpha: Float): Color {
    return set(r, g, b, alpha)
}

package app.fireflygames.library.extensions

import com.badlogic.gdx.graphics.Color

/**
 * ${FILE_NAME}
 * Zagg
 *
 * Created by kvanry on 5/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
fun Color(r: Int, g: Int, b: Int, a: Int = 255): Color {
    return Color(r / 255f, g / 255f, b / 255f, a / 255f)
}


/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Returns h, s, and l in the set [0, 1].
 *
 * @return  Array           The HSL representation
 */
val Color.hsl: HSLColor
    get() {
        val max = arrayListOf(r, g, b).max()!!
        val min = arrayListOf(r, g, b).min()!!
        val s: Float
        var h = (max + min) / 2
        val l = (max + min) / 2

        if (max == min) {
            h = 0f
            s = 0f // achromatic
        } else {
            val d = max - min
            s = if (l > 0.5f) d / (2 - max - min) else d / (max + min)

            when (max) {
                r -> {
                    h = (g - b) / d + if (g < b) 6 else 0
                }
                g -> {
                    h = (b - r) / d + 2
                }
                b -> {
                    h = (r - g) / d + 4
                }
            }

            h /= 6
        }

        return HSLColor(h, s, l)
    }

data class HSLColor(val hue: Float, val saturation: Float, val light: Float)

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  l       The lightness
 * @return  Array           The RGB representation
 */
val HSLColor.rgb: Color
    get() {
        val r: Float
        val g: Float
        val b: Float

        if (saturation == 0f) {
            r = light
            g = light
            b = light
        } else {

            val q = if (light < 0.5) light * (1 + saturation) else light + saturation - light * saturation
            val p = 2 * light - q

            r = hue2rgb(p, q, hue + 1 / 3)
            g = hue2rgb(p, q, hue)
            b = hue2rgb(p, q, hue - 1 / 3)
        }

        return Color(r, g, b, 1f)
    }


fun hue2rgb(p: Float, q: Float, _t: Float): Float {
    var t = _t
    if (t < 0) t += 1
    if (t > 1) t -= 1
    if (t < 1 / 6) return p + (q - p) * 6 * t
    if (t < 1 / 2) return q
    if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6
    return p
}

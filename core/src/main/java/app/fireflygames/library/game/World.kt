package app.fireflygames.library.game

import app.fireflygames.library.game_objects.GameObject
import app.fireflygames.library.view.TouchEvent
import app.fireflygames.library.view.View
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.Viewport
import java.util.*

/**
 * Create new [World]
 * @param camera camera from [GameScreen]
 */
@Suppress("UNUSED")
class World (var camera: OrthographicCamera, var viewport: Viewport, var foregroundCamera: OrthographicCamera, var foregroundViewport: Viewport) {

    private var layerGameObjects = true
    private var clickDown = false
    private var foregroundClickDown = false

    var gameObjects = ArrayList<GameObject>()
    private var gameObjectsToAdd = ArrayList<GameObject>()
    private var gameObjectsToRemove = ArrayList<GameObject>()
    var views: MutableList<View> = ArrayList()
    private var viewsToAdd = ArrayList<View>()
    private var viewsToRemove = ArrayList<View>()

    private var viewFocus: View? = null
    private var gameObjectFocus: GameObject? = null
    private var touchEvent: TouchEvent = TouchEvent()

    val viewWidth: Float
        get() = foregroundViewport.worldWidth

    val viewHeight: Float
        get() = foregroundViewport.worldHeight

    val width: Float
        get() = viewport.worldWidth

    val height: Float
        get() = viewport.worldHeight

    /**
     * update the world every frame using this method
     * @param delta time since last update
     */
    fun update(delta: Float) {
        updateGameObjects(delta)
        updateViews(delta)
    }

    private fun updateGameObjects(delta: Float) {
        if (gameObjectsToAdd.isNotEmpty()) {
            gameObjects.addAll(gameObjectsToAdd)
            sortGameObjects()
        }
        gameObjectsToAdd.clear()
        gameObjects.forEach { it.update(delta) }
        gameObjects.removeAll(gameObjectsToRemove)
        gameObjects.removeAll { it.needsRemoval }
        gameObjectsToRemove.clear()
    }

    private fun updateViews(delta: Float) {
        if (viewsToAdd.isNotEmpty()) {
            views.addAll(viewsToAdd)
            views.sort()
        }
        viewsToAdd.clear()
        views.forEach { it.update(delta) }
        views.removeAll(viewsToRemove)
    }

    /**
     * updates the world based on touch input
     * for best results update before the master update
     * @param touchPos position of mouse or touch point if on mobile
     * *
     * @param touchDown is clicking or if currently touching screen
     */
    fun updateTouchInput(touchPos: Vector3, touchDown: Boolean): Boolean {
        var returnVal = false
        touchEvent.x = touchPos.x
        touchEvent.y = touchPos.y
        if (touchDown && !clickDown) {
            returnVal = onTouchDown()
        } else if (!touchDown && clickDown) {
            returnVal = onTouchUp()
        } else if (touchDown && clickDown) {
            returnVal = onTouchMoved()
        }
//        gameObjects.forEach { it.updateTouchInput(touchPos, touchDown) }
        clickDown = touchDown
        return returnVal
    }

    /**
     * updates the world based on touch input
     * for best results update before the master update
     * @param touchPos position of mouse or touch point if on mobile
     * *
     * @param touchDown is clicking or if currently touching screen
     */
    fun updateForegroundTouchInput(touchPos: Vector3, touchDown: Boolean): Boolean {
        var returnValue = false
        if (!views.isEmpty()) {
            touchEvent.x = touchPos.x
            touchEvent.y = touchPos.y
            if (touchDown && !foregroundClickDown) {
                returnValue = onTouchDownForeground()
            } else if (!touchDown && foregroundClickDown) {
                returnValue = onTouchUpForeground()
            } else if (touchDown && foregroundClickDown) {
                returnValue = onTouchMovedForeground()
            }
            foregroundClickDown = touchDown
        }
        return returnValue
    }

    /**
     * Add a [GameObject] to be handled by [World]
     * @param gameObject2D [GameObject] to be handled
     */
    fun addGameObject(gameObject2D: GameObject) {
        this.gameObjectsToAdd.add(gameObject2D)
    }

    private fun sortGameObjects() {
        if (layerGameObjects) {
            gameObjects.sort()
        }
    }

    /**
     * Add a [View] to be handled by [World]
     * @param view [View] to be handled
     */
    fun addView(view: View) {
        this.viewsToAdd.add(view)
    }

    /**
     * Remove a game object from the screen
     * @param gameObject [GameObject] to remove
     */
    fun removeGameObject(gameObject: GameObject) {
        gameObjectsToRemove.add(gameObject)
    }

    /**
     * Remove a view from the screen
     * @param view [View] to remove
     */
    fun removeView(view: View) {
        viewsToRemove.add(view)
    }

    private fun onTouchDown(): Boolean {
        touchEvent.action = TouchEvent.Action.DOWN
        gameObjects.reversed().firstOrNull { it.isTouching(touchEvent) }?.dispatchTouchEvent(touchEvent)
        return false
    }

    private fun onTouchUp(): Boolean {
        touchEvent.action = TouchEvent.Action.UP
        if (gameObjectFocus != null) {
            gameObjectFocus!!.dispatchTouchEvent(touchEvent)
            gameObjectFocus = null
            return true
        }
        return false
    }

    private fun onTouchMoved(): Boolean {
        touchEvent.action = TouchEvent.Action.MOVE
        return gameObjectFocus?.dispatchTouchEvent(touchEvent) ?: false
    }

    private fun onTouchDownForeground(): Boolean {
        touchEvent.action = TouchEvent.Action.DOWN
        views.reversed()
                .filter { it.isTouching(touchEvent) }
                .forEach { if (it.dispatchTouchEvent(touchEvent)) return true }
        return false
    }

    private fun onTouchUpForeground(): Boolean {
        touchEvent.action = TouchEvent.Action.UP
        if (viewFocus != null) {
            viewFocus!!.dispatchTouchEvent(touchEvent)
            viewFocus = null
            return true
        }
        return false
    }

    private fun onTouchMovedForeground(): Boolean {
        touchEvent.action = TouchEvent.Action.MOVE
        return viewFocus?.dispatchTouchEvent(touchEvent) ?: false
    }

    fun dispose() {
        for (gameObject in gameObjects) {
            gameObject.dispose()
        }
    }

    fun requestFocus(view: View) {
        for (v in views) {
            v.clearFocus()
        }
        viewFocus = view
        viewFocus!!.focus = true
    }

    fun requestFocus(gameObject: GameObject) {
        gameObjects.forEach { it.clearFocus() }
        gameObjectFocus = gameObject
        gameObjectFocus!!.focus = true
    }

}

package app.fireflygames.library.game

import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
 * Created by kodyvanry on 5/2/17
 */

abstract class G2DSprite : com.badlogic.gdx.graphics.g2d.Sprite, Comparable<G2DSprite> {

    open var z = 0

    constructor() : super() {
        z = 0
    }

    constructor(texture: TextureRegion) : super(texture) {
        z = 0
    }

    init {
        texture?.let {
            super.setTexture(it)
        }
    }

    override fun compareTo(other: G2DSprite): Int {
        return z - other.z
    }

    open fun setWidth(width: Float) {
        super.setSize(width, height)
    }

    open fun setHeight(height: Float) {
        super.setSize(width, height)
    }

    abstract fun update(delta: Float)
}

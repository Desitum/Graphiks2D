package app.fireflygames.library.game

import com.badlogic.gdx.graphics.g2d.Batch

/**
 * Created by kody on 12/27/15.
 * can be used by kody and people in [kody}]
 */
class WorldRenderer(var world: World) {

    fun draw(batch: Batch) {
        world.gameObjects.forEach { it.draw(batch, world.viewport) }
        batch.flush()
    }

    fun drawForeground(batch: Batch) {
        world.views.forEach { it.draw(batch, world.foregroundViewport) }
        batch.flush()
    }
}

package app.fireflygames.library.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import app.fireflygames.library.drawing.Drawable
import java.util.*

/**
 * Created by kodyvanry on 5/11/17
 */

@Suppress("UNUSED")
class AssetManager private constructor() {

    private var mTextureList: ArrayList<Texture> = ArrayList()
    private val mDrawableMap: HashMap<Int, Drawable> = HashMap()
    private val mBitmapFonts: HashMap<Int, BitmapFont> = HashMap()
    private val mSounds: HashMap<Int, Sound> = HashMap()
    private val mMusic: HashMap<Int, Music> = HashMap()

    private fun addTexture(texture: Texture) {
        mTextureList.add(texture)
    }

    fun addFont(key: Int, bitmapFont: BitmapFont) {
        mBitmapFonts[key] = bitmapFont
    }

    fun addTexture(texture: String) {
        this.addTexture(Texture(texture))
    }

    fun getTexture(text: Int): Texture {
        return mTextureList[text]
    }

    fun getFont(key: Int): BitmapFont {
        return mBitmapFonts[key]!!
    }

    fun addDrawable(key: Int, textureRegion: TextureRegion) {
        mDrawableMap[key] = Drawable(textureRegion)
    }

    fun addDrawable(key: Int, texture: Texture) {
        mDrawableMap[key] = Drawable(texture)
    }

    fun addDrawable(key: Int, ninePatch: NinePatch) {
        mDrawableMap[key] = Drawable(ninePatch)
    }

    fun addDrawable(key: Int, drawable: Drawable) {
        mDrawableMap[key] = drawable
    }


    fun getDrawable(key: Int): Drawable {
        return Drawable(mDrawableMap[key].let { it!! })
    }

    fun addSound(key: Int, sound: Sound) {
        mSounds[key] = sound
    }

    fun addSound(key: Int, location: String) {
        mSounds[key] = Gdx.audio.newSound(Gdx.files.internal(location))
    }


    fun getSound(key: Int): Sound {
        return mSounds[key]!!
    }

    fun addMusic(key: Int, sound: Music) {
        mMusic[key] = sound
    }

    fun addMusic(key: Int, location: String) {
        mMusic[key] = Gdx.audio.newMusic(Gdx.files.internal(location))
    }


    fun getMusic(key: Int): Music {
        return mMusic[key]!!
    }

    companion object {

        @JvmStatic val instance: AssetManager by lazy { AssetManager() }

        @JvmStatic fun dispose() {
            val assetManager: AssetManager = instance
            for (texture in assetManager.mTextureList) {
                texture.dispose()
            }
        }
    }
}

package app.fireflygames.library.particles

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureRegion
import app.fireflygames.library.game.G2DSprite

/**
 * ParticleV2.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/1/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
class ParticleV2(texture: TextureRegion, width: Float, height: Float) : G2DSprite(texture) {
    private var currentLife: Float = 0f
    private var lifespan: Float = 1f
    private var velocityX: Float = 0f
    private var velocityY: Float = 0f
    var accelerationX: Float = 0f
    var accelerationY: Float = 0f
    var rotationSpeed: Float = 0f
    var scaleSpeed: Float = 0f

    var particleColorBlue = 0f
    var particleColorGreen = 0f
    var particleColorRed = 0f
    var particleColorAlpha = 1f

    var particleColorBlueSpeed = 0f
    var particleColorGreenSpeed = 0f
    var particleColorRedSpeed = 0f
    var particleColorAlphaSpeed = 0f

    private val currentColor: Color
        get() = Color(
                particleColorRed + (particleColorRedSpeed * currentLife),
                particleColorGreen + (particleColorGreenSpeed * currentLife),
                particleColorBlue + (particleColorBlueSpeed * currentLife),
                particleColorAlpha + (particleColorAlphaSpeed * currentLife)
        )

    var remove: Boolean = false
        @JvmName("needToRemove") get
        private set

    override fun update(delta: Float) {
        currentLife += delta
        if (currentLife >= lifespan) {
            currentLife = lifespan
            remove = true
        }
        velocityX += accelerationX * delta
        velocityY += accelerationY * delta
        this.x += velocityX * delta
        this.y += velocityY * delta
        this.rotation += rotationSpeed * delta
        this.setScale(scaleX + scaleSpeed * delta)

        setColor(currentColor)
    }

    fun setup(lifespan: Float,
              accelerationX: Float,
              accelerationY: Float,
              velocityX: Float,
              velocityY: Float,
              rotation: Float,
              rotationSpeed: Float,
              scale: Float,
              scaleSpeed: Float,
              alpha: Float,
              fadeOut: Boolean,
              particleColor: Color = Color.WHITE,
              particleColorBlueSpeed: Float,
              particleColorGreenSpeed: Float,
              particleColorRedSpeed: Float) {

        this.remove = false
        this.currentLife = 0f
        this.lifespan = lifespan
        this.accelerationX = accelerationX
        this.accelerationY = accelerationY
        this.velocityX = velocityX
        this.velocityY = velocityY
        this.rotation = rotation
        this.rotationSpeed = rotationSpeed
        this.setScale(scale, scale)
        this.scaleSpeed = scaleSpeed
        this.rotationSpeed = rotationSpeed
        this.particleColorAlpha = alpha
        if (fadeOut) particleColorAlphaSpeed = -(particleColorAlpha / lifespan)
        this.color = particleColor
        this.particleColorRed = color.r
        this.particleColorGreen = color.g
        this.particleColorBlue = color.b
        this.particleColorBlueSpeed = particleColorBlueSpeed
        this.particleColorGreenSpeed = particleColorGreenSpeed
        this.particleColorRedSpeed = particleColorRedSpeed

        setOrigin(width / 2, height / 2)
    }

    init {
        setSize(width, height)
    }
}


package app.fireflygames.library.particles

import app.fireflygames.library.extensions.float
import app.fireflygames.library.extensions.radians
import app.fireflygames.library.extensions.region
import app.fireflygames.library.game.World
import app.fireflygames.library.game_objects.GameObject
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.viewport.Viewport
import com.google.gson.Gson
import java.util.*

/**
 * Emitter.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/1/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
@Suppress("MemberVisibilityCanBePrivate")
open class Emitter @JvmOverloads constructor(_world: World? = null) : GameObject(null, _world) {

    var birthRate: Float = 0f
    var maxParticles: Int = 0
    var particleLifetime: Float = 1f
    var particleLifetimeRange: Float = 0f
    var particleVelocity: Float = 0f
    var particleVelocityRange: Float = 0f
    var emissionAngle: Float = 0f
    var emissionAngleRange: Float = 0f
    var accelerationX: Float = 0f
    var accelerationY: Float = 0f
    var particleRotation: Float = 0f
    var particleRotationRange: Float = 0f
    var particleRotationSpeed: Float = 0f
    var particleScale: Float = 1f
    var particleScaleRange: Float = 0f
    var particleScaleSpeed: Float = 0f
    var particleTexture: TextureRegion? = null
    var particleWidth: Float = 0f
    var particleHeight: Float = 0f

    var posX: Float
        get() = x
        set(value) {
            x = value
        }
    var posY: Float
        get() = y
        set(value) {
            y = value
        }

    var xRange: Float = 0f
    var yRange: Float = 0f
    var zPosition: Int = 0

    var particleColor: Color = Color.WHITE
    var particleColorBlueSpeed = 0f
    var particleColorGreenSpeed = 0f
    var particleColorRedSpeed = 0f

    var particleAlpha = 1f
    var particleAlphaRange = 0f
    var fadeOut = true
    var particle: String = ""
    var hexParticleColor: String = ""


    var particles: ArrayList<ParticleV2> = arrayListOf()
        private set
    private var deadParticles: ArrayList<ParticleV2> = arrayListOf()
    var on: Boolean = true
        private set
    private var currentTime: Float = 0f

    val randomParticleX: Float
        get() = posX - (xRange / 2) + (xRange * Math.random()).float

    val randomParticleY: Float
        get() = posY - (yRange / 2) + (yRange * Math.random()).float

    val randomVelocity: Float
        get() = particleVelocity - (particleVelocityRange / 2) + (particleVelocityRange * Math.random()).float

    override fun update(delta: Float) {
        updateParticles(delta)
        updateEmitter(delta)
    }

    fun reset() {
        particles.clear()
    }

    private fun updateParticles(delta: Float) {
        particles.forEach {
            it.update(delta)
            if (it.remove)
                deadParticles.add(it)
        }
        particles.removeAll { it.remove }
    }

    private fun updateEmitter(delta: Float) {
        if (on) {
            currentTime += delta
            while (currentTime >= 1.0f / birthRate && (particles.size <= maxParticles || maxParticles == 0)) {
                val particle = createNewParticle()
                particle.color = color
                addParticle(particle)

                currentTime -= 1.0f / birthRate
            }
        }
    }

    fun addParticle(particle: ParticleV2) {
        particle.color = color
        particles.add(particle)
    }

    /**
     * Recycles old particle or if there are no spare mParticles, it creates a new one
     * @return [ParticleV2]
     */
    open fun createNewParticle(): ParticleV2 {
        val particle: ParticleV2 = if (deadParticles.size > 0) {
            deadParticles.removeAt(0)
        } else {
            ParticleV2(particleTexture!!, particleWidth, particleHeight)
        }
        val randomVelocity = this.randomVelocity
        particle.setup(particleLifetime - (particleLifetimeRange / 2) + (particleLifetimeRange * Math.random()).float,
                accelerationX,
                accelerationY,
                randomVelocity * Math.cos((emissionAngle - (emissionAngleRange / 2) + (emissionAngleRange * Math.random())).radians).float,
                randomVelocity * Math.sin((emissionAngle - (emissionAngleRange / 2) + (emissionAngleRange * Math.random())).radians).float,
                particleRotation - (particleRotationRange / 2) + (particleRotationRange * Math.random()).float,
                particleRotationSpeed,
                particleScale - (particleScaleRange / 2) + (particleScaleRange * Math.random()).float,
                particleScaleSpeed,
                particleAlpha - (particleAlphaRange / 2) + (particleAlphaRange * Math.random()).float,
                fadeOut,
                particleColor,
                particleColorBlueSpeed,
                particleColorGreenSpeed,
                particleColorRedSpeed
        )
        particle.setPosition(randomParticleX, randomParticleY)
        return particle
    }

    override fun onDraw(spriteBatch: Batch, viewport: Viewport) {
        super.onDraw(spriteBatch, viewport)
        particles.forEach { it.draw(spriteBatch) }
    }

    fun turnOn() {
        on = true
    }

    fun turnOff() {
        on = false
    }

    fun toggleOnOff() {
        on = !on
    }

    companion object {
        @JvmStatic
        fun fromFile(fileNamed: String): Emitter {
            val json = Gdx.files.internal(fileNamed).readString()
            val jsonParser = Gson()
            val emitter = jsonParser.fromJson(json, Emitter::class.java)
            emitter.particleColor = Color.valueOf(emitter.hexParticleColor)
            emitter.particleTexture = Texture(emitter.particle).region
            emitter.z = emitter.zPosition
            print(emitter)
            return emitter
        }
    }
}

package app.fireflygames.library.interpolation

/**
 * RollerCoasterInterpolator.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/17/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */

import com.badlogic.gdx.utils.Pool

/**
 * An interpolator where the rate of change starts out slowly and then accelerates over time.
 *
 * @author Kody Van Ry
 */
class RollerCoasterInterpolator internal constructor()// hide constructor
    : Interpolator {

    private var factor: Float = DEFAULT_FACTOR

    override fun finished() {
        pool.free(this)
    }

    override fun getInterpolation(input: Float): Float {
        return factor*(-Math.cos(2*Math.PI*input)+1).toFloat()
    }

    override fun copy(): Interpolator {
        return get(factor)
    }

    companion object {

        private val DEFAULT_FACTOR = 1.0f

        private val pool = object : Pool<RollerCoasterInterpolator>(4, 100) {
            override fun newObject(): RollerCoasterInterpolator {
                return RollerCoasterInterpolator()
            }
        }

        /**
         * Gets a new [app.fireflygames.library.interpolation.AccelerateInterpolator] from a maintained pool of [Interpolator]s.
         *
         * @param factor the factor controlling the rate of change
         * @return the obtained [app.fireflygames.library.interpolation.AccelerateInterpolator]
         */
        @JvmOverloads
        fun get(factor: Float = DEFAULT_FACTOR): RollerCoasterInterpolator {
            val inter = pool.obtain()
            inter.factor = factor
            return inter
        }
    }
}
/**
 * Gets a new [app.fireflygames.library.interpolation.AccelerateInterpolator] from a maintained pool of [Interpolator]s.
 *
 *
 * The initial factor is set to `{@value app.fireflygames.library.interpolation.AccelerateInterpolator#DEFAULT_FACTOR}`.
 *
 * @return the obtained [app.fireflygames.library.interpolation.AccelerateInterpolator]
 */


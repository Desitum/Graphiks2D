package app.fireflygames.library.interpolation;

import com.badlogic.gdx.utils.Pool;

/**
 * An interpolator where the rate of change starts out slowly, grows over time and ends slowly.
 *
 * @author Moritz Post <moritzpost@gmail.com>
 */
public class AccelerateDecelerateInterpolator implements Interpolator {

    private static final float DEFAULT_FACTOR = 1.0f;

    private static final Pool<AccelerateDecelerateInterpolator> pool = new Pool<AccelerateDecelerateInterpolator>(4, 100) {
        @Override
        protected AccelerateDecelerateInterpolator newObject() {
            return new AccelerateDecelerateInterpolator();
        }
    };

    private float factor;

    private AccelerateDecelerateInterpolator() {
        // hide constructor
    }

    /**
     * Gets a new {@link app.fireflygames.library.interpolation.AccelerateDecelerateInterpolator} from a maintained pool of {@link Interpolator}s.
     *
     * @param factor the factor controlling the rate of speed change
     * @return the obtained {@link app.fireflygames.library.interpolation.AccelerateDecelerateInterpolator}
     */
    public static AccelerateDecelerateInterpolator get(float factor) {
        AccelerateDecelerateInterpolator inter = pool.obtain();
        inter.factor = factor;
        return inter;
    }

    /**
     * Gets a new {@link app.fireflygames.library.interpolation.AccelerateDecelerateInterpolator} from a maintained pool of {@link Interpolator}s.
     * <p/>
     * The initial factor is set to <code>{@value app.fireflygames.library.interpolation.AccelerateDecelerateInterpolator#DEFAULT_FACTOR}</code>.
     *
     * @return the obtained {@link app.fireflygames.library.interpolation.AccelerateDecelerateInterpolator}
     */
    public static AccelerateDecelerateInterpolator get() {
        return get(DEFAULT_FACTOR);
    }

    @Override
    public void finished() {
        pool.free(this);
    }

    public float getInterpolation(float input) {
        return (float) (Math.cos((input + 1) * Math.PI) / 2.0f) + 0.5f;
    }

    @Override
    public Interpolator copy() {
        return get(factor);
    }
}

package app.fireflygames.library.interpolation

import com.badlogic.gdx.utils.Pool

/**
 * BoingInterpolator
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/21/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */

/**
 * An interpolator that makes your sprites go Boing :)
 *
 * @author Moritz Post <moritzpost></moritzpost>@gmail.com>
 */
class BoingInterpolator internal constructor()// hide constructor
    : Interpolator {

    private var bounces: Float = DEFAULT_BOUNCES
    private var factor: Float = DEFAULT_FACTOR

    override fun finished() {
        pool.free(this)
    }

    override fun getInterpolation(input: Float): Float {
        return (-Math.sin(bounces * Math.PI * input) * (1 - input)).toFloat() * factor
    }

    override fun copy(): Interpolator {
        return get(bounces, factor)
    }

    companion object {

        private const val DEFAULT_BOUNCES = 6f
        private const val DEFAULT_FACTOR = 1f

        private val pool = object : Pool<BoingInterpolator>(4, 100) {
            override fun newObject(): BoingInterpolator {
                return BoingInterpolator()
            }
        }

        /**
         * Gets a new [app.fireflygames.library.interpolation.BoingInterpolator] from a maintained pool of [Interpolator]s.
         *
         * @param factor the factor controlling the rate of change
         * @return the obtained [app.fireflygames.library.interpolation.BoingInterpolator]
         */
        @JvmOverloads
        fun get(bounces: Float = DEFAULT_BOUNCES, factor: Float = DEFAULT_FACTOR): BoingInterpolator {
            val inter = pool.obtain()
            inter.bounces = bounces
            inter.factor = factor
            return inter
        }
    }
}

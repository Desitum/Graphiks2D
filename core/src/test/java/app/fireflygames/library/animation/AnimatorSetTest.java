package app.fireflygames.library.animation;

import app.fireflygames.library.interpolation.Interpolation;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by kodyvanry on 7/3/17.
 */

public class AnimatorSetTest {

    private MovementAnimator movementAnimator = new MovementAnimator(null, 1f, 1f, 1f, 1f, 1f, 1f, app.fireflygames.library.interpolation.Interpolation.ACCELERATE_INTERPOLATOR);
    private MovementAnimator movementAnimator2 = new MovementAnimator(null, 1f, 0f, 1f, 1f, 1f, 1f, app.fireflygames.library.interpolation.Interpolation.ACCELERATE_INTERPOLATOR);
    private AnimatorSet animatorSet;

    @Before
    public void before() {
        animatorSet = new AnimatorSet(movementAnimator, movementAnimator2);
    }

    @Test
    public void testCreate() {
        AnimatorSet animatorSet = new AnimatorSet(movementAnimator, movementAnimator2);
        assertNotNull(animatorSet);
    }

    @Test
    public void testSize() {
        assertEquals(2, animatorSet.getSize());
    }

    @Test
    public void testTotalDuration() {
        assertEquals(2f, animatorSet.getTotalDuration(), Float.MIN_VALUE);
    }

    @Test
    public void testGetAnimators() {
        assertArrayEquals(animatorSet.getAnimators(), new Animator[] {movementAnimator, movementAnimator2});
    }

    @Test
    public void testUpdate() {
        animatorSet.start();
        animatorSet.update(0.5f);
        assertTrue(animatorSet.getAnimators()[0].isRunning());
        assertEquals(0.5f, animatorSet.getAnimators()[0].getCurrentDelay(), Float.MIN_VALUE);
        assertEquals(0.5f, animatorSet.getAnimators()[1].getTimeInAnimation(), Float.MIN_VALUE);
        assertNotEquals(0.5f, animatorSet.getAnimators()[0].getTimeInAnimation(), Float.MIN_VALUE);
    }

    @Test
    public void testDuplicate() {
        Animator animatorSet1 = animatorSet.clone();
        assertTrue(animatorSet1 instanceof AnimatorSet);
        assertNotSame(animatorSet, animatorSet1);
    }

}

package app.fireflygames.library.animation

import app.fireflygames.library.interpolation.Interpolation
import app.fireflygames.library.view.LayoutConstraints
import app.fireflygames.library.view.View

import org.junit.Test
import org.junit.Assert.*

/**
 * Created by kodyvanry on 7/4/17.
 */

class MarginAnimatorTest {

    @Test
    fun createTest() {
        val layoutConstraints = LayoutConstraints(0f, 0f, 1f, 1f, 0f, 1f, 2f, 1f, 2f, true, true, true, true, false)
        val layoutConstraints1 = LayoutConstraints(0f, 0f, 1f, 1f, 0f, 2f, 1f, 2f, 1f, true, true, true, true, false)
        val marginAnimator = MarginAnimator(null, layoutConstraints, layoutConstraints1, 1f, Interpolation.LINEAR_INTERPOLATOR)
        assertNotNull(marginAnimator)
    }

    @Test
    fun animationTest() {
        val v = View(null, null)
        val layoutConstraints = LayoutConstraints(0f, 0f, 1f, 1f, 0f, 1f, 1f, 2f, 2f, true, true, true, true, false)
        val layoutConstraints1 = LayoutConstraints(0f, 0f, 1f, 1f, 0f, 2f, 2f, 1f, 1f, true, true, true, true, false)
        val marginAnimator = MarginAnimator(v, layoutConstraints, layoutConstraints1, 1f, Interpolation.LINEAR_INTERPOLATOR)

        marginAnimator.start()
        marginAnimator.update(.25f)
        marginAnimator.updateAnimation()
        assertNotNull(marginAnimator.sprite)
        assertEquals(1.25f, (marginAnimator.sprite as View).layoutConstraints.marginStart, EPSILON)
        assertEquals(1.25f, (marginAnimator.sprite as View).layoutConstraints.marginEnd, EPSILON)
        assertEquals(1.75f, (marginAnimator.sprite as View).layoutConstraints.marginTop, EPSILON)
        assertEquals(1.75f, (marginAnimator.sprite as View).layoutConstraints.marginBottom, EPSILON)

        marginAnimator.update(.5f)
        marginAnimator.updateAnimation()
        assertNotNull(marginAnimator.sprite)
        assertEquals(1.75f, (marginAnimator.sprite as View).layoutConstraints.marginStart, EPSILON)
        assertEquals(1.75f, (marginAnimator.sprite as View).layoutConstraints.marginEnd, EPSILON)
        assertEquals(1.25f, (marginAnimator.sprite as View).layoutConstraints.marginTop, EPSILON)
        assertEquals(1.25f, (marginAnimator.sprite as View).layoutConstraints.marginBottom, EPSILON)
    }

    companion object {

        private const val EPSILON = 0.00001f
    }
}

package app.fireflygames.library.animation

import com.badlogic.gdx.graphics.Color

import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

/**
 * Created by kodyvanry on 7/3/17.
 */

class ColorEffectsTest {

    private var red: Color? = null
    private var green: Color? = null
    private var blue: Color? = null

    @Before
    fun before() {
        red = Color.RED
        green = Color.GREEN
        blue = Color.BLUE
    }

    @Test
    fun createTest() {
        val colorEffects = ColorEffects(null, 1f, 0f, red!!, green!!)
        assertNotNull(colorEffects)
        val colorEffects1 = ColorEffects(null, 1f, 1f, red!!, green!!)
        assertNotNull(colorEffects1)
    }

    @Test
    fun slopeTest() {
        val colorEffects = ColorEffects(null, 1f, 0f, red!!, green!!)
        colorEffects.timeInAnimation = 1f
        colorEffects.updateAnimation()
        assertEquals(1.0f, colorEffects.currentGreen, java.lang.Float.MIN_VALUE)
        assertEquals(0.0f, colorEffects.currentRed, java.lang.Float.MIN_VALUE)
    }

    @Test
    fun updateTest() {
        val colorEffects = ColorEffects(null, 1f, 0f, red!!, green!!)
        var updatedTimes = 0
        colorEffects.onAnimationUpdated = {
            updatedTimes += 1
        }
        colorEffects.start()

        colorEffects.update(0.25f)
        assertEquals(0.75f, colorEffects.currentRed, EPSILON)
        assertEquals(0.25f, colorEffects.currentGreen, EPSILON)
        assertEquals(0f, colorEffects.currentBlue, EPSILON)
        assertEquals(1f, colorEffects.currentAlpha, EPSILON)
        assertEquals(1, updatedTimes)

        colorEffects.update(0.330000000f)
        assertEquals(0.42f, colorEffects.currentRed, EPSILON)
        assertEquals(0.58f, colorEffects.currentGreen, EPSILON)
        assertEquals(0f, colorEffects.currentBlue, EPSILON)
        assertEquals(1f, colorEffects.currentAlpha, EPSILON)
        assertEquals(2, updatedTimes)
    }

    @Test
    fun duplicateTest() {
        val colorEffects = ColorEffects(null, 1f, 0f, blue!!, green!!)
        val colorEffects1 = colorEffects.clone()

        assertTrue(colorEffects1 is ColorEffects)
        assertEquals(colorEffects, colorEffects1) // According to .equals they are the same
        assertFalse(colorEffects === colorEffects1) // According to their reference they are different
    }

    @Test
    fun colorsMatchTest() {
        assertTrue(ColorEffects.colorsMatch(Color.RED, Color.RED, 0.0f))
        assertTrue(ColorEffects.colorsMatch(Color.RED, Color(1f, 0f, 0.999f, 1f), 1.0f))
        assertFalse(ColorEffects.colorsMatch(Color.BLUE, Color(1f, 1f, 0f, 1f), 1.0f))
    }

    companion object {

        private val EPSILON = 0.00001f
    }

}

package app.fireflygames.library.desktop;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import app.fireflygames.library.BuildMenu;
import app.fireflygames.library.LibraryTest;
import app.fireflygames.library.game.AssetManager;

import app.fireflygames.library.LibraryTest;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//        Game buildMenu = new BuildMenu();
        new LwjglApplication(new LibraryTest(), config);
    }
}

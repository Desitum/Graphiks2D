package app.fireflygames.library.android

import android.content.Context
import android.os.Bundle
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * ${FILE_NAME}
 * Graphiks2D
 *
 * Created by kvanry on 6/17/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
class RewardedVideoAd(val adId: String, val name: String) {


    var isSetup = false
    var isReady = false
    var onResult: (Boolean) -> Unit = {}
    var deservesReward: Boolean = false

    lateinit var ad: RewardedVideoAd

    fun setup(context: Context) {
        if (!isSetup) {
            ad = MobileAds.getRewardedVideoAdInstance(context)
            ad.rewardedVideoAdListener = object : RewardedVideoAdListener {
                override fun onRewardedVideoAdClosed() {
                    loadRewardedVideoAd(context)
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, adId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(INTERSTITIAL_AD_CLICK_EVENT, bundle)
                    onResult(deservesReward)
                }

                override fun onRewardedVideoAdLeftApplication() {
                }

                override fun onRewardedVideoAdLoaded() {
                    isReady = true
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, adId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(VIDEO_LOADED, bundle)
                }

                override fun onRewardedVideoAdOpened() {
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, adId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(VIDEO_OPENED, bundle)
                }

                override fun onRewardedVideoCompleted() {
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, adId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(VIDEO_COMPLETED, bundle)
                    deservesReward
                }

                override fun onRewarded(p0: RewardItem?) {
                    deservesReward = true
                }

                override fun onRewardedVideoStarted() {
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, adId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(VIDEO_STARTED, bundle)
                }

                override fun onRewardedVideoAdFailedToLoad(p0: Int) {
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, adId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(VIDEO_AD_FAILED, bundle)
                }
            }
            ad.setImmersiveMode(true)
            loadRewardedVideoAd(context)
            isSetup = true
        }
    }

    fun showAd(onResult: (Boolean) -> Unit) {
        this.deservesReward = false
        this.onResult = onResult
        ad.show()
    }

    private fun loadRewardedVideoAd(context: Context) {
        ad.loadAd(if (LIB_DEBUG) context.getString(R.string.debug_video_ad) else adId,
                AdRequest.Builder().build())
    }
}

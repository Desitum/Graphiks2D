package app.fireflygames.library.android

import com.android.billingclient.api.*

/**
 * BillingManager.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/14/18.
 * Copyright (c) 2018. FireFly Game Labs. All rights reserved worldwide.
 */
class BillingManager(private val manager: AndroidManager) : PurchasesUpdatedListener {

    private var billingClient: BillingClient = BillingClient.newBuilder(manager.context).setListener(this).build()

    private var listeners = hashMapOf<String, (Boolean, InAppPurchase) -> Unit>()

    init {
        startBillingConnection()
    }

    private fun startBillingConnection(onResult: (success: Boolean) -> Unit = {}) {
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    queryPurchaseData()
                    onResult(true)
                }
                onResult(false)
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                onResult(false)
            }
        })
    }

    fun consume(inAppPurchase: InAppPurchase, onResult: (Boolean, InAppPurchase) -> Unit) {
        if (!billingClient.isReady) {
            startBillingConnection { success ->
                if (success)
                    consume(inAppPurchase, onResult)
            }
            return
        }

        if (inAppPurchase.purchaseToken == "" || !inAppPurchase.consumable) {
            onResult(false, inAppPurchase)
        }

        consumePurchase(inAppPurchase, onResult)
    }

    fun purchase(sku: String, onResult: (Boolean, InAppPurchase) -> Unit) {
        val flowParams = BillingFlowParams.newBuilder()
                .setSku(sku)
                .setType(BillingClient.SkuType.INAPP) // SkuType.SUB for subscription
                .build()
        listeners.put(sku, onResult)
        billingClient.launchBillingFlow(manager.context, flowParams)
    }

    private fun consumePurchase(inAppPurchase: InAppPurchase, onResult: (Boolean, InAppPurchase) -> Unit) {
        val listener = ConsumeResponseListener { responseCode, _ ->
            if (responseCode == BillingClient.BillingResponse.OK) {
                inAppPurchase.consumed = true
                onResult(true, inAppPurchase)
            } else {
                onResult(false, inAppPurchase)
            }
        }

        billingClient.consumeAsync(inAppPurchase.purchaseToken, listener)
    }

    private fun queryPurchaseData() {
        updateSkuData()
        updatePurchases()
    }

    private fun updateSkuData() {
        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(manager.context.skuList.map { it.sku }).setType(BillingClient.SkuType.INAPP)
        billingClient.querySkuDetailsAsync(params.build(), { responseCode, skuDetailsList ->
            if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
                for (skuDetails in skuDetailsList) {
                    val sku = skuDetails.sku
                    val price = skuDetails.price
                    manager.context.skuList.find { it.sku == sku }?.price = price
                }
            }
        })
    }


    private fun updatePurchases() {
        billingClient.queryPurchases(BillingClient.SkuType.INAPP).purchasesList.forEach { purchase ->
            manager.context.skuList.find { purchase.sku == it.sku }?.let {
                it.consumed = false
                it.purchased = true
                it.purchaseToken = purchase.purchaseToken
            }
        }
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        purchases?.forEach { purchase ->
            manager.context.skuList.find { it.sku == purchase.sku }?.let { sku ->
                sku.purchaseToken = purchase.purchaseToken
                sku.purchased = true
                sku.consumed = false
                listeners[purchase.sku]?.invoke(true, sku)
            }
        }
    }
}

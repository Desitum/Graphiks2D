package app.fireflygames.library.android

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.badlogic.gdx.Game
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.games.AchievementsClient
import com.google.android.gms.games.Games
import com.google.android.gms.games.LeaderboardsClient
import com.google.firebase.analytics.FirebaseAnalytics


/**
 * AndroidManager.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/6/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
@Suppress("UNUSED")
class AndroidManager(val context: AndroidGame, gameName: String, interstitialCrossSaleLoaded: () -> Unit, landscapeCrossSaleLoaded: () -> Unit) {

    var gameRunning: Boolean = false
    private var playCount: Int = 0

    private var googleSignInClient: GoogleSignInClient
    private var leaderboardClient: LeaderboardsClient? = null
    private var achievementsClient: AchievementsClient? = null
    private val firebaseManager: FirebaseManager
    private val billingManager: BillingManager?

    init {
        firebaseManager = loadFirebaseManager(gameName, interstitialCrossSaleLoaded, landscapeCrossSaleLoaded)

        billingManager = loadBillingManager()

        googleSignInClient = GoogleSignIn.getClient(context, GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)

        setupAds()
    }

    private fun loadFirebaseManager(gameName: String, crossSaleLoaded: () -> Unit, landscapeCrossSaleLoaded: () -> Unit): FirebaseManager {
        return FirebaseManager(context, gameName) {
            if (it.interstitials.isNotEmpty()) {
                crossSaleLoaded()
            }
            if (it.landscapes.isNotEmpty()) {
                landscapeCrossSaleLoaded()
            }
        }
    }

    private fun loadBillingManager(): BillingManager? {
        return if (context.skuList.isNotEmpty()) {
            BillingManager(this)
        } else {
            null
        }
    }

    fun showLeaderboard(leaderboardId: Int) {
        leaderboardClient?.getLeaderboardIntent(context.getString(leaderboardId))
                ?.addOnSuccessListener { intent -> context.startActivityForResult(intent, RC_LEADERBOARD_UI); }
    }

    fun showAllLeaderboards() {
        leaderboardClient?.allLeaderboardsIntent
                ?.addOnSuccessListener { intent -> context.startActivityForResult(intent, RC_LEADERBOARD_UI); }
    }

    fun submitScore(leaderboardId: Int, score: Int) {
        leaderboardClient?.submitScore(context.getString(leaderboardId), score.toLong())
    }

    fun unlockAchievement(achievementId: Int) {
        achievementsClient?.unlock(context.getString(achievementId))
    }

    fun showAchievements() {
        achievementsClient?.achievementsIntent
                ?.addOnSuccessListener { intent -> context.startActivityForResult(intent, RC_ACHIEVEMENT_UI); }
    }

    fun incrementAchievement(achievementId: Int) {
        achievementsClient?.increment(context.getString(achievementId), 1)
    }

    fun incrementAchievement(achievementId: Int, amount: Int) {
        achievementsClient?.increment(context.getString(achievementId), amount)
    }

    fun share(message: String, title: Int) {
        context.startActivity(Intent.createChooser(getShareIntent(message), context.getText(title)))
    }

    private fun startSignInIntent() {
        context.startActivityForResult(googleSignInClient.signInIntent, RC_SIGN_IN)
    }

    private fun setupAds() {
        MobileAds.initialize(context, context.appAdId)
        context.interstitialAds.forEach { ad ->
            ad.setup(context)
        }
        context.videoAds.forEach { ad ->
            ad.setup(context)
        }
    }

    fun gameStarted() {
        gameRunning = true
        playCount += 1

        val bundle = Bundle()
        bundle.putString(APP_ID, context.packageName)
        bundle.putInt(PLAY_COUNT, playCount)
        FirebaseAnalytics.getInstance(context).logEvent(PLAY_COUNT, bundle)
    }

    fun onConnected(googleSignInAccount: GoogleSignInAccount) {
        Log.d(TAG, "onConnected(): connected to Google APIs")
        leaderboardClient = Games.getLeaderboardsClient(context, googleSignInAccount)
        achievementsClient = Games.getAchievementsClient(context, googleSignInAccount)
    }

    fun onDisconnected() {
        Log.d(TAG, "onDisconnected()")

        leaderboardClient = null
        achievementsClient = null
    }

    fun signInSilently() {
        Log.d(TAG, "signInSilently()")

        googleSignInClient.silentSignIn().addOnCompleteListener(context) { task ->
            if (task.isSuccessful) {
                Log.d(TAG, "signInSilently(): success")
                onConnected(task.result)
            } else {
                startSignInIntent()
                Log.d(TAG, "signInSilently(): failure", task.exception)
                onDisconnected()
            }
        }
    }

    fun showInterstitialAd(byName: String) {
        context.apply {
            runOnUiThread {
                interstitialAds.find { it.name == byName }?.ad?.let { interstitialAd ->
                    if (interstitialAd.isLoaded) {
                        interstitialAd.show()
                    }
                }
            }
        }
    }

    fun showInterstitialAdById(byId: String) {
        context.apply {
            runOnUiThread {
                interstitialAds.find { it.adId == byId }?.ad?.let { interstitialAd ->
                    if (interstitialAd.isLoaded) {
                        interstitialAd.show()
                    }
                }
            }
        }
    }

    fun showVideoAd(byName: String, onResult: (Boolean) -> Unit) {
        context.apply {
            runOnUiThread {
                videoAds.find { it.name == byName }?.let { videoAd ->
                    if (videoAd.isReady) {
                        videoAd.showAd(onResult)
                    }
                }
            }
        }
    }

    fun isVideoReady(byName: String): Boolean {
        context.apply {
            videoAds.find { it.name == byName }?.let { videoAd ->
                return videoAd.isReady
            }
        }
        return false
    }

    fun showVideoAdById(byId: String, onResult: (Boolean) -> Unit) {
        context.apply {
            runOnUiThread {
                videoAds.find { it.adId == byId }?.let { videoAd ->
                    if (videoAd.isReady) {
                        videoAd.showAd(onResult)
                    }
                }
            }
        }
    }

    fun purchase(inAppPurchase: InAppPurchase, onResult: (Boolean, InAppPurchase) -> Unit) {
        billingManager?.purchase(inAppPurchase.sku, onResult)
    }

    fun getInterstitialCrossSale(): CrossSale? {
        if (firebaseManager.interstitials.isEmpty()) return null
        return firebaseManager.interstitials.random
    }

    fun getLandscapeCrossSale(): CrossSale? {
        if (firebaseManager.landscapes.isEmpty()) return null
        return firebaseManager.landscapes.random
    }

    fun showSignInException(apiException: ApiException) {
        val message = apiException.message
        if (message != null) {
            AlertDialog.Builder(context)
                    .setMessage(message)
                    .setNeutralButton(android.R.string.ok, null)
                    .show()
        }
    }

    fun consume(inAppPurchase: InAppPurchase, onResult: (Boolean, InAppPurchase) -> Unit) {
        billingManager?.consume(inAppPurchase, onResult)
    }

    companion object {
        private const val TAG = "AndroidManager"
        private const val RC_LEADERBOARD_UI = 1213
        private const val RC_ACHIEVEMENT_UI = 1234
        const val RC_SIGN_IN = 1212
    }
}

fun getShareIntent(message: String): Intent {
    val sendIntent = Intent()
    sendIntent.action = Intent.ACTION_SEND
    sendIntent.putExtra(Intent.EXTRA_TEXT, message)
    sendIntent.type = "text/plain"
    return sendIntent
}

fun AndroidApplication.createGameView(game: Game, configuration: AndroidApplicationConfiguration): View {
    val gameView = initializeForView(game, configuration)
    val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
    params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE)
    gameView.layoutParams = params
    return gameView
}

private val ArrayList<CrossSale>.random: CrossSale
    get() {
        return get((Math.random() * size).toInt())
    }
package app.fireflygames.library.android

import android.content.Context
import android.os.Bundle
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.firebase.analytics.FirebaseAnalytics

class Graphiks2DInterstitialAd(val adId: String, val name: String) {

    var isSetup = false

    lateinit var ad: InterstitialAd

    fun setup(context: Context) {
        if (!isSetup) {
            ad = InterstitialAd(context)
            ad.adUnitId = if (LIB_DEBUG) context.getString(R.string.debug_interstitial_ad_id) else adId
            ad.loadAd(AdRequest.Builder().build())
            ad.adListener = object : AdListener() {
                override fun onAdClosed() {
                    ad.loadAd(AdRequest.Builder().build())
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, ad.adUnitId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(INTERSTITIAL_AD_CLICK_EVENT, bundle)
                }

                override fun onAdFailedToLoad(p0: Int) {
                    super.onAdFailedToLoad(p0)
                    val bundle = Bundle()
                    bundle.putString(AD_UNIT_ID, ad.adUnitId)
                    bundle.putString(APP_ID, context.packageName)
                    FirebaseAnalytics.getInstance(context).logEvent(INTERSTITIAL_AD_FAILED, bundle)
                }
            }
            ad.setImmersiveMode(true)
            isSetup = true
        }
    }

}

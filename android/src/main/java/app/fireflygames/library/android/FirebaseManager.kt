package app.fireflygames.library.android

import android.content.Context
import android.preference.PreferenceManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.GenericTypeIndicator



/**
 * FirebaseManager.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/11/18.
 * Copyright (c) 2018. FireFly Labs. All rights reserved worldwide.
 */
class FirebaseManager(val context: Context, val gameName: String, val onNewData: (FirebaseManager) -> Unit) {

    val database = FirebaseDatabase.getInstance()
    var interstitials = arrayListOf<CrossSale>()
    var landscapes = arrayListOf<CrossSale>()

    init {
        loadCrossSale()
    }

    fun showCrossSale() {
        loadInterstitials()
        loadLandscapes()
    }

    private fun loadInterstitials() {
        database.getReference("games").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                p0.toException()?.printStackTrace()
            }

            override fun onDataChange(p0: DataSnapshot) {
                val t = object : GenericTypeIndicator<ArrayList<CrossSale>>() {}

                p0.getValue(t)?.let {
                    interstitials = ArrayList(it.filter { it.name != gameName })
                }
                onNewData(this@FirebaseManager)
            }

        })
    }

    private fun loadLandscapes() {
        database.getReference("landscape_games").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                p0.toException()?.printStackTrace()
            }

            override fun onDataChange(p0: DataSnapshot) {
                val t = object : GenericTypeIndicator<ArrayList<CrossSale>>() {}

                p0.getValue(t)?.let {
                    landscapes = ArrayList(it.filter { it.name != gameName })
                }
                onNewData(this@FirebaseManager)
            }

        })
    }

    private fun loadCrossSale() {
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        if (sharedPrefs.getInt(CROSS_SALE_COUNT, 0) > 2) {
            sharedPrefs.edit().putInt(CROSS_SALE_COUNT, 0).apply()
            showCrossSale()
        }
        sharedPrefs.edit().putInt(CROSS_SALE_COUNT, sharedPrefs.getInt(CROSS_SALE_COUNT, 0) + 1).apply()
    }

    companion object {
        const val CROSS_SALE_COUNT = "cross_sale"
    }
}

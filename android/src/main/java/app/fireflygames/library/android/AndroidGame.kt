package app.fireflygames.library.android

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.annotation.StringRes
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import app.fireflygames.library.android.AndroidManager.Companion.RC_SIGN_IN
import com.badlogic.gdx.Game
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.launcher.*

@Suppress("Unused", "PLUGIN_WARNING", "MemberVisibilityCanBePrivate")
abstract class AndroidGame : AndroidApplication() {

    lateinit var androidManager: AndroidManager

    lateinit var game: Game
    private lateinit var gameView: View

    abstract val appAdId: String

    abstract val gameName: String

    abstract val skuList: ArrayList<InAppPurchase>

    abstract val onCrossInterstitialCrossSaleLoaded: () -> Unit

    abstract val onLandscapeCrossSaleLoaded: () -> Unit

    var interstitialAds = arrayListOf<Graphiks2DInterstitialAd>()
    var videoAds = arrayListOf<RewardedVideoAd>()

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        game = createGame()
        val layout = layoutInflater.inflate(R.layout.launcher, null) as RelativeLayout
        gameView = createGameView(game, configuration)
        layout.addView(gameView)
        setContentView(layout)

        androidManager = AndroidManager(this, gameName, onCrossInterstitialCrossSaleLoaded, onLandscapeCrossSaleLoaded)
    }


    private val configuration: AndroidApplicationConfiguration
        get() {
            val config = AndroidApplicationConfiguration()
            config.useImmersiveMode = true
            config.hideStatusBar = true
            config.useAccelerometer = false
            config.useCompass = false
            return config
        }

    fun showLeaderboard(leaderboardId: Int) {
        androidManager.showLeaderboard(leaderboardId)
    }

    fun showLeaderboards() {
        androidManager.showAllLeaderboards()
    }

    @SuppressLint("ApplySharedPref")
    fun submitScore(leaderboardId: Int, score: Int) {
        androidManager.submitScore(leaderboardId, score)
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(getString(leaderboardId), Math.max(score, getScore(leaderboardId))).commit()
    }

    fun unlockAchievement(achievementId: Int) {
        androidManager.unlockAchievement(achievementId)
    }

    fun showAchievements() {
        androidManager.showAchievements()
    }

    fun incrementAchievement(achievementId: Int) {
        androidManager.incrementAchievement(achievementId)
    }

    fun incrementAchievement(achievementId: Int, amount: Int) {
        androidManager.incrementAchievement(achievementId, amount)
    }

    fun getScore(leaderboardId: Int): Int {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(getString(leaderboardId), 0)
    }

    fun share(message: String) {
        androidManager.share(message, R.string.share)
    }

    @SuppressLint("ApplySharedPref")
    fun storeLocalInt(@StringRes id: Int, value: Int) {
        storeLocalInt(getString(id), value)
    }

    @SuppressLint("ApplySharedPref")
    fun storeLocalInt(key: String, value: Int) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).commit()
    }

    fun getLocalInt(@StringRes id: Int): Int{
        return getLocalInt(id, 0)
    }

    fun getLocalInt(@StringRes id: Int, defaultValue: Int): Int{
        return getLocalInt(getString(id), defaultValue)
    }

    fun getLocalInt(key: String): Int{
        return getLocalInt(key, 0)
    }

    fun getLocalInt(key: String, defaultValue: Int): Int{
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaultValue)
    }

    @SuppressLint("ApplySharedPref")
    fun storeLocalBoolean(@StringRes id: Int, value: Boolean) {
        storeLocalBoolean(getString(id), value)
    }

    @SuppressLint("ApplySharedPref")
    fun storeLocalBoolean(key: String, value: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).commit()
    }

    fun getLocalBoolean(@StringRes id: Int): Boolean{
        return getLocalBoolean(id, false)
    }

    fun getLocalBoolean(@StringRes id: Int, defaultValue: Boolean): Boolean{
        return getLocalBoolean(getString(id), defaultValue)
    }

    fun getLocalBoolean(key: String): Boolean{
        return getLocalBoolean(key, false)
    }

    fun getLocalBoolean(key: String, defaultValue: Boolean): Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue)
    }

    @SuppressLint("ApplySharedPref")
    fun storeLocalString(@StringRes id: Int, value: String) {
        storeLocalString(getString(id), value)
    }

    @SuppressLint("ApplySharedPref")
    fun storeLocalString(key: String, value: String) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).commit()
    }

    fun getLocalString(@StringRes id: Int): String {
        return getLocalString(id, "")
    }

    fun getLocalString(@StringRes id: Int, defaultValue: String): String {
        return getLocalString(getString(id), defaultValue)
    }

    fun getLocalString(key: String): String {
        return getLocalString(key, "")
    }

    fun getLocalString(key: String, defaultValue: String): String {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue)
    }

    override fun onStart() {
        super.onStart()
        androidManager.signInSilently()
    }


    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(intent)
            try {
                val account = task.getResult(ApiException::class.java)
                androidManager.onConnected(account)
            } catch (apiException: ApiException) {
                androidManager.onDisconnected()

                showSignInError(apiException)
            }
        }
    }

    private fun showSignInError(apiException: ApiException) {
        if (LIB_DEBUG) {
            androidManager.showSignInException(apiException)
        }
    }

    fun showInterstitialAd(name: String) {
        androidManager.showInterstitialAd(name)
    }

    fun showVideoAd(name: String, onResult: (Boolean) -> Unit) {
        androidManager.showVideoAd(name, onResult)
    }

    fun purchase(inAppPurchase: InAppPurchase, onResult: (Boolean, InAppPurchase) -> Unit) {
        androidManager.purchase(inAppPurchase, onResult)
    }

    fun consume(inAppPurchase: InAppPurchase, onResult: (Boolean, InAppPurchase) -> Unit) {
        androidManager.consume(inAppPurchase, onResult)
    }

    fun gameStarted() {
        androidManager.gameStarted()
    }

    fun askForRating(title: String, message: String) {
        showRatingDialog(title, message, R.string.rate, R.string.not_now, R.string.never)
    }

    fun getLandscapeCrossSale() = androidManager.getLandscapeCrossSale()

    fun showInterstitialCrossSale() {
        androidManager.getInterstitialCrossSale()?.let {
            showInterstitialCrossSale(it)
        }
    }

    private fun showInterstitialCrossSale(crossSale: CrossSale) {
        when (crossSale.layout) {
            1 -> showLayout1CrossSale(crossSale)
        }
    }

    private fun showLayout1CrossSale(crossSale: CrossSale) {
        layout1.visibility = View.VISIBLE
        layout1.setOnClickListener { clickedOnLink(crossSale.androidLink) }
        layout1PlayButton.setOnClickListener { clickedOnLink(crossSale.androidLink) }
        layout1Image.loadImage(crossSale.imageUrl)
        layout1CloseButton.setOnClickListener { layout1.visibility = View.GONE }
    }

    override fun onBackPressed() {
        layout1.visibility = View.GONE
    }

    private fun clickedOnLink(link: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        startActivity(browserIntent)
    }

    companion object {
        private const val TAG = "AndroidLauncher"
    }

    private fun ImageView.loadImage(link: String) {
        GlideApp.with(this).load(link).centerCrop().into(this)
    }

    abstract fun createGame(): Game
}

var LIB_DEBUG = true

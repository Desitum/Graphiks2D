package app.fireflygames.library.android

/**
 * InAppPurchase.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 6/14/18.
 * Copyright (c) 2018. FireFly Games. All rights reserved worldwide.
 */
class InAppPurchase(val sku: String, var price: String = "", var purchaseToken: String = "", var purchased: Boolean = false, var consumable: Boolean = false, var consumed: Boolean = true, onUpdated: (InAppPurchase) -> Unit)

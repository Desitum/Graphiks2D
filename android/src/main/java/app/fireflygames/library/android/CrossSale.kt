package app.fireflygames.library.android

/**
 * CrossSale.kt
 * Graphiks2D
 *
 * Created by Kody Van Ry on 5/20/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */
class CrossSale @JvmOverloads constructor(
        var androidLink: String = "",
        var imageUrl: String = "",
        var layout: Int = 0,
        var name: String = ""
)

package app.fireflygames.library.android

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import com.badlogic.gdx.backends.android.AndroidApplication

/**
 * RatingManager.kt
 * Graphiks2D
 *
 * Created by kvanry on 5/23/18.
 * Copyright (c) 2018. Firefly Game Labs. All rights reserved worldwide.
 */

fun askForRating(context: Context) {
    val uri = Uri.parse("market://details?id=" + context.packageName)
    val goToMarket = Intent(Intent.ACTION_VIEW, uri)
    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
            Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
            Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
    try {
        context.startActivity(goToMarket)
    } catch (e: ActivityNotFoundException) {
        context.startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse("http://play.google.com/store/apps/details?id=" + context.packageName)))
    }

}

var askedForRating = false

fun AndroidApplication.showRatingDialog(title: String, message: String, positiveId: Int, notNowId: Int, neverId: Int) {
    runOnUiThread {
        if (!PreferenceManager.getDefaultSharedPreferences(context).getBoolean(ASKED_FOR_RATING, false) && !askedForRating) {
            askedForRating = true
            AlertDialog.Builder(this).apply {
                setTitle(title)
                setMessage(message)
                setPositiveButton(positiveId) { _, _ ->
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(ASKED_FOR_RATING, true).apply()
                    askForRating(this@showRatingDialog)
                }
                setNegativeButton(notNowId) { _, _ ->
                }
                setNeutralButton(neverId) { _, _ ->
                    PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(ASKED_FOR_RATING, true).apply()
                }
            }.show()
        }
    }
}

private const val ASKED_FOR_RATING = "rate_on_google"

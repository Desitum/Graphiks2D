package app.fireflygames.library.android

/**
 * ${FILE_NAME}
 * Graphiks2D
 *
 * Created by kvanry on 6/8/18.
 * Copyright (c) 2018. Desitum. All rights reserved worldwide.
 */
const val INTERSTITIAL_AD_CLICK_EVENT = "interstitial_click"
const val VIDEO_OPENED = "video_opened"
const val VIDEO_LOADED = "video_loaded"
const val VIDEO_COMPLETED = "video_completed"
const val VIDEO_STARTED = "video_completed"
const val VIDEO_AD_FAILED = "video_completed"
const val INTERSTITIAL_AD_FAILED = "interstitial_failed"
const val PLAY_COUNT = "play_count"
const val AD_UNIT_ID = "ad_id"
const val APP_ID = "app_id"